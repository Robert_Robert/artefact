<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ShopperDiscounts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('shopper_discounts', function (Blueprint $table) {
         $table->increments('id');
         $table->integer('shopper_id');
         $table->integer('discount_percentage');
         $table->date('date_start');
         $table->date('date_end');
     });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
          Schema::drop('shopper_siscounts');
    }
}
