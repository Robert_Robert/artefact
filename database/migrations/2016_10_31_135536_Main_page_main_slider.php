<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MainPageMainSlider extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('main_page_main_slider', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image');
            $table->string('ru_description')->nullable();
            $table->string('ua_description')->nullable();
            $table->string('en_description')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('main_page_main_slider');
    }
}
