<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Product extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ua_name');
            $table->string('ua_description', 1000);
            $table->string('ru_name');
            $table->string('ru_description', 1000);
            $table->string('en_name');
            $table->string('en_description', 1000);
            $table->float('price');
            $table->integer('category')->unsigned()->index();
            $table->foreign('category')->references('id')->on('main_category')->onDelete('cascade');
            $table->integer('sub_category')->unsigned()->index();
            $table->foreign('sub_category')->references('id')->on('sub_category')->onDelete('cascade');
            $table->timestamp('date');

            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('product');
    }
}
