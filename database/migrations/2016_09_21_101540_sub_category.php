<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SubCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::create('sub_category', function($table)
    {
         $table->increments('id');
         $table->string('ua_name');
         $table->string('ru_name');
         $table->string('en_name');
         $table->integer('main_category')->unsigned()->index();
         $table->foreign('main_category')->references('id')->on('main_category')->onDelete('cascade');;

    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sub_category');
    }
}
