<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ShopperPurchases extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('shopper_purchases', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('shopper_id')->unsigned()->index();
            $table->foreign('shopper_id')->references('id')->on('shoppers')->onDelete('cascade');
            $table->integer('product_id')->unsigned()->index();
            $table->foreign('product_id')->references('id')->on('product')->onDelete('cascade');
            $table->integer('product_quant');
            $table->integer('product_price');
            $table->integer('total_product_price');
            $table->string('status', 50);
            $table->dateTime('created_at');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('shopper_purchases');
    }
}
