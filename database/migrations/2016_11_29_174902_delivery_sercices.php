<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeliverySercices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('delivery_services', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ru_name', 100);
            $table->string('ua_name', 100);
            $table->string('en_name', 100);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('delivery_services');
    }
}
