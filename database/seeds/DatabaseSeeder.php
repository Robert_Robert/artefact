<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('main_category')->insert([
         'ua_name' =>'Декор',
         'ru_name' =>'Декор',
         'en_name' =>'Decor',
         'image'   =>'public/category_images/category_581c471c33410.jpg'
     ]);
     DB::table('main_category')->insert([
        'ua_name' =>'Меблі',
        'ru_name' =>'Мебель',
        'en_name' =>'Furniture',
        'image'   =>'public/category_images/category_decor.jpg'
    ]);
    DB::table('main_category')->insert([
       'ua_name' =>'Освітлення',
       'ru_name' =>'Освещение',
       'en_name' =>'Illumination', 
       'image'   =>'public/category_images/category_581a259cb9c63.jpg'
   ]);
    }
}
