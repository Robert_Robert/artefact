<?php

return array(
    'welcome' => 'Welcome to Artefact',
    'search'  => 'Result of search',
    'add_to_basket' => 'Add to basket',
    'basket'  => 'Basket',
    'news'    => 'Novelties',
    'search'  => 'Search',
    'search_line'=>'Your request',
    'main'    => 'Main',
    'services'=> 'Services',
    'about'   => 'About as',
    'shop'    => 'Shop',
    'blog'    => 'Blog',
    'contacts'=> 'Contacts',
    'decor'   => 'Decor',
    'illumination' => 'illumination',
    'furniture' => 'Furniture',
    'created' => 'Created by',
    'to_order'=> 'To order own design',
    'communication'=> 'We always in communication',
    'welcome' => 'Welcome',
    'adres'  => 'Ukraine, Chernivtsy, Golovna 198а street, of. 210',
    'call'    => 'Call us',
    'message' => 'Write message',
    'write'   => 'e-mail',
    'who'     => 'Who we are',
    'what'    => 'What we can?',
    'adoutt'  => 'About',
    'monitoring'=>'Start monitoring',
    'go_to'   => 'Go to ',
    'buy'     => 'Buy something beautiful',
    'review'  => 'Review',
    'opportunity' => 'Rate our opportunity',
    'shopp'   => 'Shop',
    'country' => 'Country',
    'city'    => 'City',
    'region'  => 'Region',
    'index'   => 'Index',
    'adress'  => 'Adress',
    'name'    => 'Name',
    'surname' => 'Surname',
    'email'   => 'Email',
    'phone'   => 'Phone',
    'all'     => 'All',
    'sort'    => 'Sort by',
    'biggest_price' => 'Highest price',
    'lowest_price' => 'Lowest price',
    'date'    => 'Date',
    'prev'    => 'Previous',
    'next'    => 'Next',
    'send'    => 'Send',
    'your_message' => 'Your message',




);
