<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>ARTEFACT ADMIN</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->

  <link rel="stylesheet" href="{{ asset('resources/assets/admin/bootstrap/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('resources/assets/admin/dist/css/additional_style.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('resources/assets/admin/dist/css/AdminLTE.css') }}">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect.
  -->
  <link rel="stylesheet" href="{{ asset('resources/assets/admin/dist/css/skins/skin-blue.min.css') }}">

  <!-- Morrsi chart styles -->
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">

  <link rel="stylesheet" href="{{ asset('resources/assets/admin/plugins/datepicker/css/bootstrap-datepicker.css') }}" media="screen" title="no title">

  <!-- jQuery 2.2.0 -->
  <script src="{{ asset('resources/assets/admin/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>

  <!-- Bootstrap 3.3.6 -->
  <script src="{{ asset('resources/assets/admin/bootstrap/js/bootstrap.min.js') }}"></script>

  <!-- Morris Diadgram -->
  <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
  <script src="{{url('resources/assets/admin/plugins/morris/morris.min.js')}}"></script>


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->




</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
  <header class="main-header">

    <!-- Logo -->
    <a href="{{ url('/admin') }}" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini">ARTEFACT</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>ARTEFACT</b>ADMIN</span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <a href="{{ url('/') }}" target="_blank"><button type="button" class="btn " style="height: 52px; margin-left: 35%;     font-size: x-large;">ARTEFACT</button></a>

      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">

      @if (Auth::check())
        <a href="{{ url('/admin/exit') }}"><button type="button" class="btn btn-danger" style="height: 52px;">Выйти</button></a>
        @endif
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">


      <ul class="sidebar-menu">
        <li class="header">HEADER</li>
        <!-- Optionally, you can add icons to the links -->
        <li class="active"><a href="{{ url('/admin/orders_history') }}"><i class="fa fa-link"></i> <span>История заказов</span></a></li>
         <li class="active"><a href="{{ url('/admin/delivery') }}"><i class="fa fa-link"></i> <span>Службы доставки</span></a></li>
         <li class="treeview">
          <a href="#"><i class="fa fa-link"></i> <span>Категории</span> <i class="fa fa-angle-left pull-right"></i></a>
          <ul class="treeview-menu">
            <li><a href="{{ url('/admin/category') }}">Добавить категорию</a></li>
            <li><a href="{{ url('/admin/category-list') }}">Редактировать категорию</a></li>
            <li><a href="{{ url('/admin/sub-category') }}">Добавить подкатегорию</a></li>
          </ul>
        </li>
        <li class="active"><a href="{{ url('/admin/product') }}"><i class="fa fa-link"></i> <span>Добавить товар</span></a></li>


         <li class="treeview">
          <a href="#"><i class="fa fa-link"></i> <span>Редактирование товаров</span> <i class="fa fa-angle-left pull-right"></i></a>
          <ul class="treeview-menu">
          @foreach(App\Models\Main_category::get_all_category() as $category)
            <li><a href="{{ url('/admin/products_list/'.$category->id) }}">{{ $category->ru_name }}</a></li>

          @endforeach
          </ul>
        </li>

        <li class="treeview">
          <a href="#"><i class="fa fa-link"></i> <span>Управление главной</span> <i class="fa fa-angle-left pull-right"></i></a>
          <ul class="treeview-menu">
            <li><a href="{{ url('/admin/main_slider') }}">Слайдер</a></li>
            <li><a href="{{ url('/admin/category-list') }}">О нас</a></li>
            <li><a href="{{ url('/admin/slider') }}">Начните следить</a></li>
            <li><a href="{{ url('/admin/sub-category') }}">Магазин</a></li>
            <li><a href="{{ url('/admin/sub-category') }}">Услуги</a></li>
          </ul>
        </li>
         <li class="treeview">
          <a href="#"><i class="fa fa-link"></i> <span>Статистика</span> <i class="fa fa-angle-left pull-right"></i></a>
          <ul class="treeview-menu">
            <li><a href="{{ url('admin/client_statistic') }}">По клиенту</a></li>
            <li><a href="{{ url('admin/date_statistic') }}">По времени</a></li>
          </ul>
        </li>
        <li class="treeview">
         <a href="#"><i class="fa fa-link"></i> <span>Скидки</span> <i class="fa fa-angle-left pull-right"></i></a>
         <ul class="treeview-menu">
           <li><a href="{{ url('admin/dicount_form') }}">Добавить скидку</a></li>
           <li><a href="{{ url('admin/discount_list') }}">Список добавленых скидок</a></li>
         </ul>
       </li>
        <li class="treeview">
          <a href="#"><i class="fa fa-link"></i> <span>Блог</span> <i class="fa fa-angle-left pull-right"></i></a>
          <ul class="treeview-menu">
            <li>
              <li class="treeview">
          <a href="#"><i class="fa fa-link"></i> <span>Добавить статью</span> <i class="fa fa-angle-left pull-right"></i></a>
          <ul class="treeview-menu">
            <li><a href="{{ url('/admin/blog/ua-article') }}">Добавити на українській </a></li>
            <li><a href="{{ url('/admin/blog/ru-article') }}">Добавить на русском</a></li>
            <li><a href="{{ url('/admin/blog/en-article') }}">Add on english</a></li>
          </ul>
        </li>
            </li>
            <li class="treeview">
          <a href="#"><i class="fa fa-link"></i> <span>Править статью</span> <i class="fa fa-angle-left pull-right"></i></a>
          <ul class="treeview-menu">
         <li><a href="{{ url('admin/blog/ua-articles') }}">Редагувати на українській </a></li>
            <li><a href="{{ url('admin/blog/ru-articles') }}">Править на русском</a></li>
            <li><a href="{{ url('admin/blog/en-articles') }}">Edit on english</a></li>
          </ul>
        </li>
          </ul>
        </li>
        <li class="active"><a href="{{ url('/register') }}"><i class="fa fa-link"></i> <span>Добавить администратора</span></a></li>



      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->


    <section class="content-header">
      <h1>

       @yield('content-header')

      </h1>
    </section>


    <section class="content">

        @yield('content')


    </section>


  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2016 <a href="#">ARTEFACT</a></strong> All rights reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane active" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript::;">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript::;">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="label label-danger pull-right">70%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->


<!-- AdminLTE App -->
<script src="{{ asset('resources/assets/admin/dist/js/app.min.js') }}"></script>
<script src="{{ asset('resources/assets/admin/dist/js/myscript.js') }}" id="myscript"></script>
<script src="{{ asset('resources/assets/tinymce/js/tinymce/tinymce.js') }}"></script>
<!-- DatePicker -->
<script src="{{ asset('resources/assets/admin/plugins/datepicker/js/bootstrap-datepicker.js') }}"></script>
   <script>tinymce.init({


     selector: 'textarea',
    height : "250",
    toolbar: "image",
    plugins: [
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table contextmenu paste code',
    'image'
  ],
  toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image ',
   file_browser_callback: function(field_name, url, type, win) {
        if(type=='image') $('#my_form input').click();
    }
    });



    $(document).ready(function(){
      $('#start_date').datepicker({
         format: "yyyy-mm-dd",
         clearBtn: true,
         language: "ru",
         autoclose: true
       });

       $('#end_date').datepicker({
          format: "yyyy-mm-dd",
          clearBtn: true,
          language: "ru",
          autoclose: true
        });
   });

    </script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. Slimscroll is required when using the
     fixed layout. -->
</body>
</html>
