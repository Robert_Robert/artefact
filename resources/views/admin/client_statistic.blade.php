@extends('admin.admin-layout')

       @section('content-header')
        <h1>Статистика покупок по клиенту</h1>
       @stop


        @section('content')

      <div class="panel panel-default">
       <div class="panel-body">
         <form action='{{url("admin/client_statistic")}}' method="GET">
           
         <h4>Выберете клиента</h4>
            <select class="form-control" style="border-radius: 5px;" name="client">
            @if(isset($client))
                @foreach($shoppers as $shopper)
                  <option value="{{$shopper->id}}" @if($shopper->id == $client) selected @endif>{{$shopper->surname}} {{$shopper->name}} </option>
                @endforeach
            @else   
            @foreach($shoppers as $shopper)
                  <option value="{{$shopper->id}}">{{$shopper->surname}} {{$shopper->name}} </option>
                @endforeach 
            @endif    
            </select>


         <h4>Выберете дату</h4>
         <div class="input-group" style="width: 40%; float: left;">
          <span class="input-group-addon" id="basic-addon1">Начальная дата</span>
           <input type="text" name="start_date" class="form-control" placeholder="yyyy-mm-dd" aria-describedby="basic-addon1" id="start_date">
         </div>



         <div class="input-group"  style="width: 40%; float: right;">
          <input type="text" name="end_date" class="form-control" placeholder="yyyy-mm-dd" aria-describedby="basic-addon1" id="end_date">
          <span class="input-group-addon" id="basic-addon2">Конечная дата</span>
        </div>
         <input  class="btn btn-primary" style="width: 20%;" type="submit" name="calculate" value="Узнать">
        <div style='clear:both;'></div>
      </form>

       @if(isset($calculate))
      <div class="box box-default" style="margin-top: 10px; width: 30%; display: inline-block; ">
        <h3>Количество товаров</h3>
         <div class="box-body">
           <div id="sales-chart"></div>
           <script>

                     //DONUT CHART
                     var donut = new Morris.Donut({
                       element: 'sales-chart',
                       resize: true,
                       colors: ["#0000EE", "#FF0000", "#CD00CD"],
                       data: [
                         {label: "Декор", value: {{$decor}} },
                         {label: "Освещение", value: {{$iluminate}} },
                         {label: "Мебель", value: {{$furniture}} }
                       ],
                       hideHover: 'auto'
                     });

           </script>
         </div><!-- /.box-body -->
      </div><!-- /.box -->
        @endif

       @if(isset($calculate))
      <div class="box box-default" style="margin-top: 10px; width: 30%; display: inline-block;" id="diagram_finance">
          <h3>Затрачено по-категориям</h3>
         <div class="box-body">
           <div id="money-chart"></div>
           <script>

                     //DONUT CHART
                     var donut = new Morris.Donut({
                       element: 'money-chart',
                       resize: true,
                       colors: ["#0000EE", "#FF0000", "#CD00CD"],
                       data: [
                         {label: "Декор", value: {{$decor_cash}} },
                         {label: "Освещение", value: {{$iluminate_cash}} },
                         {label: "Мебель", value: {{$furniture_cash}} }
                       ],
                       hideHover: 'auto'
                     });

           </script>
         </div><!-- /.box-body -->
      </div><!-- /.box -->
        @endif

      @if(isset($calculate))
      <div class="box box-default all-sum" style="margin-top: 10px; width: 30%;  float:right;" id="display_money">
          <h3>Всего потрачено</h3>
      <div class="col-lg-12 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>{{$total_cash}}<sup style="font-size: 20px">$</sup></h3>
          </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>

          </div>
        </div>
        <div>
        </div>
        </div><!-- /.box -->
        @endif




       </div>
     </div>

        @stop
