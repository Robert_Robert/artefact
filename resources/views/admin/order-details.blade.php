@extends('admin.admin-layout')

@section('content-header')

@stop


@section('content')

<h3 style="color: green;">{{ session('message') }}</h3>
  <h2>Детали заказа</h2>
    <table class="table table-striped">
                <tbody><tr>
                  <th style="width: 10px">#</th>
                  <th>Статус заказа</th>
                  <th>EMAIL покупателя</th>
                  <th >Сумма заказа</th>
                  <th >Сообщение</th>
                  <th>Дата</th>

                </tr>
  
                <tr>
                  <td>{{$order->id}}</td>
                  <td>
                  @if($order->status == 'оплачено')
                  <span class="badge bg-green">{{$order->status}}</span></td>
                  @else
                  <span class="badge bg-red">{{$order->status}}</span></td>
                  @endif
                  <td>
                   {{$order->user_mail}}
                  </td>
                  <td><span class="badge bg-light-blue">{{$order->order_price}}$</span></td>
                  <td>{{$order->message}}</td>
                  <td><span class="badge bg-yellow">{{$order->created_at}}
             
                  </span></td>
                </tr>
              
              </tbody></table>
             
        <h2>Детали доставки</h2> 
        <a href="{{url('/admin/edit-order-delivery/'.$delivery->id)}}"><button type="button" class="btn btn-info" id="edit_delivery">Править</button></a>

         <table class="table table-striped">
                <tbody><tr>
                  <th>Страна</th>
                  <th>Город</th>
                  <th>Область</th>
                  <th>Адрес</th>
                  <th>Почтовый индекс</th>
                  <th>Служба доставки</th>
                  <th>Адрес отделения</th>
                  <th>Имя</th>
                  <th>Фамилия</th>
                  <th>E-mail</th>
                  <th>Телефон</th>
                  

                </tr>
  
                <tr>
                  <td>{{$delivery->country}}</td>
                  <td>{{$delivery->city}}</td>
                  <td>{{$delivery->region}}</td>
                  <td>{{$delivery->adress}}</td>
                  <td>{{$delivery->post_index}}</td>
                  <td>{{$delivery->delivery_service}}</td>
                  <td>{{$delivery->delivery_department}}</td>
                  <td>{{$delivery->name}}</td>
                  <td>{{$delivery->surname}}</td>
                  <td>{{$delivery->email  }}</td>
                  <td>{{$delivery->phone}}</td>
                </tr>
              
              </tbody></table> 


              <h2>Товары</h2> 
        

         <table class="table table-striped">
                <tbody><tr>
                  <th>Наименование товара</th>
                  <th>Количестов товара</th>
                  <th>Цена за единицу</th>
                  <th>Сумарная цена</th>
                  <th>Категория</th>
                </tr>
    
                @foreach($products as $product)
                <tr>
                  <td>{{App\Models\Product::getProductName($product->product_id)}}</td>
                  <td>{{$product->product_quant}}</td>
                  <td>{{$product->product_price}}</td>
                  <td>{{$product->total_product_price}}</td>
                  <th>{{App\Models\Main_category::getCategoryName($product->category)}}</th>
                </tr>
                @endforeach
              
              </tbody></table>         

@stop

 