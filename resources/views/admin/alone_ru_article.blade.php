@extends('admin.admin-layout')

@section('content-header')
	<h1>Редактирование статьи</h1>
@stop


@section('content')
@if (count($errors) > 0)
  <div class="alert alert-danger">
    <ul>
      @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
@endif
<h3 style="color: green;">{{ session('message') }}</h3>

	<div class="panel">
	<form action="{{ url('admin/blog/edit-ru-article/'.$article->id) }}" method="POST" id="add_ru_article">
                <div class="panel-heading">
                    <div class="text-center">
                    
   					{{ csrf_field() }}
                        <div class="row">
                            <div class="col-sm-9">
                                <h3 class="pull-left"><input type="text" name="ru_name" class="form-control" value="{{ $article->ru_name }}" style="width: 300%; border-radius: 10px;"></h3>
                            </div>
                           
                        </div>
                    </div>
                </div>
                
            <div class="panel-body">
               <textarea class="form-control" name="ru_text">{{ $article->ru_text }}</textarea>
            </div>
            
            <div class="panel-footer">
              <button type="submit" class="btn btn-primary">Редактировать</button>  
            </div>
            </form>
            <iframe id="form_target" name="form_target" style="display:none"></iframe>

            <form id="my_form" action="{{ url('admin/save_image') }}" target="form_target" method="post" enctype="multipart/form-data" style="width:0px;height:0;overflow:hidden">
            {{ csrf_field() }}
            <input name="image" type="file"  class="s_img" onchange="$('#my_form').submit();this.value='';">
            </form>
        </div>
@stop