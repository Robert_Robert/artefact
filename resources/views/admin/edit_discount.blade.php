@extends('admin.admin-layout')

       @section('content-header')
          <h1>Добавить скидку для клиента</h1>
       @stop


        @section('content')
        <h3 style="color: green;">{{ session('message') }}</h3>
        @if (count($errors) > 0)
          <div class="alert alert-danger">
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
        @endif

        <div class="panel panel-default">
         <div class="panel-body">
           <form action='{{url("admin/edit_discount/".$discount->id)}}' method="post">
             {{ csrf_field() }}
             <div style="width: 50%; float: left;">
             <h4>Выберете клиента</h4>
                <select class="form-control" style="border-radius: 5px;" name="client">
                    @foreach($shoppers as $shopper)
                      <option value="{{$shopper->id}}" @if($shopper->
                      id == $discount->shopper_id) selected @endif>
                          {{$shopper->surname}} {{$shopper->name}}
                     </option>
                    @endforeach
                </select>
              </div>
              <div style="width: 50%; float: right;">
              <h4>Введите размер скидки в процентах</h4>
                 <input type="text" name="size_discount" class="form-control" placeholder="%" style="border-radius: 4px;" value="{{$discount->discount_percentage}}">
               </div>
               <div style="clear:both;"></div>

             <h4>Выберете дату</h4>
             <div class="input-group" style="width: 40%; float: left;">
              <span class="input-group-addon" id="basic-addon1">Начальная дата</span>
               <input type="text" name="start_date" class="form-control" placeholder="yyyy-mm-dd" aria-describedby="basic-addon1" id="start_date" value="{{$discount->date_start}}">
             </div>



             <div class="input-group"  style="width: 40%; float: right;">
              <input type="text" name="end_date" class="form-control" placeholder="yyyy-mm-dd" aria-describedby="basic-addon1" id="end_date" value="{{$discount->date_end}}">
              <span class="input-group-addon" id="basic-addon2">Конечная дата</span>
            </div>
             <input  class="btn btn-primary" style="width: 20%;" type="submit" name="calculate" value="Добавить">
            <div style='clear:both;'></div>

           </form>
         </div>
       </div>

        @stop
