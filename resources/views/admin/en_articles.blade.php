@extends('admin.admin-layout')

@section('content-header')
	<h1>Список статтей на английском языке</h1>
@stop


@section('content')
<h3 style="color: green;">{{ session('message') }}</h3>
@foreach ($articles as $article)

    <p class="bg-info admin-article">{{ $article->en_name }}<a href="{{ url('admin/blog/delete-en-article/'.$article->id) }}" style="float: right;"><span class="glyphicon glyphicon-remove admin-article-glyph" ></a><a <a href="{{ url('admin/blog/edit-en-article/'.$article->id) }}" style="float: right;"><span class="glyphicon glyphicon-pencil admin-article-glyph" ></a></p>
@endforeach
<?php echo $articles->links('vendor.pagination.bootstrap-4'); ?>
@stop
