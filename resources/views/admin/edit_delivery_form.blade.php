@extends('admin.admin-layout')

@section('content-header')

@stop


@section('content')

<h3 style="color: green;">{{ session('message') }}</h3>

          <form action="{{url('admin/save_delivery_changes/'.$delivery->id)}}" method="POST"> 
           {{ csrf_field() }}  
        <h2>Детали доставки</h2> 
        <button type="submit" class="btn btn-info" id="edit_delivery">Сохранить</button>
       
         <table class="table table-striped">
                <tbody><tr>
                  <th>Страна</th>
                  <th>Город</th>
                  <th>Область</th>
                  <th>Адрес</th>
                  <th>Почтовый индекс</th>
                 
                  

                </tr>
  
                <tr>
                  <td><input type="text" class="form-control" name="country" value="{{$delivery->country}}" /></td>
                  <td><input type="text" class="form-control" name="city" value="{{$delivery->city}}" /></td>
                  <td><input type="text" class="form-control" name="region" value="{{$delivery->region}}" /></td>
                  <td><input type="text" class="form-control" name="adress" value="{{$delivery->adress}}" /></td>
                  <td><input type="text" class="form-control" name="post_index" value="{{$delivery->post_index}}" /></td>
                 
                </tr>
              
              </tbody></table> 


                 <table class="table table-striped">
                <tbody><tr>
                 
                  <th>Служба доставки</th>
                  <th>Адрес отделения</th>
                  <th>Имя</th>
                  <th>Фамилия</th>
                  <th>E-mail</th>
                  <th>Телефон</th>
                  

                </tr>
  
                <tr>
                 
                  <td><input type="text" class="form-control" name="delivery_service" value="{{$delivery->delivery_service}}" /></td>
                  <td><input type="text" class="form-control" name="delivery_department" value="{{$delivery->delivery_department}}" /></td>
                  <td><input type="text" class="form-control" name="name" value="{{$delivery->name}}" /></td>
                  <td><input type="text" class="form-control" name="surname" value="{{$delivery->surname}}" /></td>
                  <td><input type="text" class="form-control" name="email" value="{{$delivery->email}}" /></td>
                  <td><input type="text" class="form-control" name="phone" value="{{$delivery->phone}}" /></td>
                </tr>
              
              </tbody></table> 


               </form>

@stop

 