@extends('admin.admin-layout')

@section('content-header')
	<h1>Добавление товара</h1>
@stop



@section('content')
@if (count($errors) > 0)
  <div class="alert alert-danger">
    <ul>
      @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
@endif
	<form action="{{ url('/admin/add-product') }}" method="POST" enctype="multipart/form-data" >
  {{ csrf_field() }}
  <div class="form-group">
    <label for="exampleInputEmail1">Назва товару</label>
    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Назва товару" name="ua_name" value="{{ old('ua_name') }}">
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Название товара</label>
    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Название товара" name="ru_name" value="{{ old('ru_name') }}" >
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Product name</label>
    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Product name" name="en_name" value="{{ old('en_name') }}">
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Опис товару</label>
    <textarea class="form-control" name="ua_description">{{ old('ua_description') }}</textarea>
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Описание товара</label>
     <textarea class="form-control" name="ru_description">{{ old('ru_description') }}</textarea>
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Product description </label>
     <textarea class="form-control" name="en_description">{{ old('en_description') }}</textarea>
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Цена в доларах США</label>
    <input type="text" class="form-control"  placeholder="price" name="price" value="{{ old('price') }}">
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Количество товара</label>
    <input type="text" class="form-control"  placeholder="количество" name="quantity" value="{{ old('quantity') }}">
  </div>
  @if (count($main_category) > 0)
    <div class="form-group" style="width: 48%; display: inline-block;">
    <label for="exampleInputEmail1">Выберете категорию</label>
    <select class="form-control" name="main_category" id="main_category"  >
     <option value="">Выберете категорию</option>

      @foreach ($main_category as $category)
               <option value="{{ $category->id }}">{{ $category->ru_name }}</option>
      @endforeach
    </select>
  </div>
 @endif
  
    <div class="form-group" style="width: 48%; display: inline-block; float: right;">
    <label for="exampleInputEmail1">Выберете под-категорию</label>
    <select class="form-control" name="sub_category" id="sub_category" >
    </select>
  </div>

<pre style="width: 48%; display: inline-block;" id="add_image_block">
  <div class="form-group" id="add_image">
  <img src="{{ asset('resources/assets/admin/dist/img/plus.png') }}" style="float: right; cursor: pointer;" id="add_img">
    <label for="exampleInputFile" style="font-size: 127%;">Фотографии товара</label>
    <input name="image[]" type="file" class="form-control"/>
  </div>
 </pre>

 <div id="add_model_block">
  <div class="form-group" id="add_schema">
  <!-- <img src="{{ asset('resources/assets/admin/dist/img/plus.png') }}" style="float: right; cursor: pointer;" id="add_sch"> -->
    <label for="exampleInputFile" style="margin-top: 7%;">Схема товара(pdf-файл)</label>
    <input name="schema[]" type="file" class="form-control"/>
    <label for="exampleInputFile" style="margin-top: 7%;">3D модель </label>
    <input name="model" type="file" class="form-control"/>
  </div>
 </div>

 <br>
  <button type="submit" class="btn btn-default">Добавить</button>
</form>
<iframe id="form_target" name="form_target" style="display:none"></iframe>

            <form id="my_form" action="{{ url('admin/save_image') }}" target="form_target" method="post" enctype="multipart/form-data" style="width:0px;height:0;overflow:hidden">
            {{ csrf_field() }}
            <input name="image" type="file"  class="s_img" onchange="$('#my_form').submit();this.value='';">
            </form>
@stop