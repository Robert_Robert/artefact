@extends('admin.admin-layout')

@section('content-header')
	<h1>Список статтей на русском языке</h1>
@stop


@section('content')
<h3 style="color: green;">{{ session('message') }}</h3>
@foreach ($articles as $article)

    <p class="bg-info admin-article">{{ $article->ru_name }}<a href="{{ url('admin/blog/delete-ru-article/'.$article->id) }}" style="float: right;"><span class="glyphicon glyphicon-remove admin-article-glyph" ></a><a <a href="{{ url('admin/blog/edit-ru-article/'.$article->id) }}" style="float: right;"><span class="glyphicon glyphicon-pencil admin-article-glyph" ></a></p>
@endforeach
<?php echo $articles->links('vendor.pagination.bootstrap-4'); ?>
@stop
