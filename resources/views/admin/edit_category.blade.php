@extends('admin.admin-layout')

@section('content-header')
	<h1>Редактирование категории</h1>
@stop


@section('content')
<h2>Редактирование категории {{ $category->ru_name }}</h2>
<h3 style="color: green;">{{ session('message') }}</h3>
	<form action="{{ url('admin/edit_category/save-change/'.$category->id) }}" method="POST"  enctype="multipart/form-data">
   {{ csrf_field() }}
  <div class="form-group">
    <label for="exampleInputEmail1">Категорія на українській мові</label>
    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="категорія" name="ua_name" value="{{ $category->ua_name }}">
  </div>
 <div class="form-group">
    <label for="exampleInputEmail1">Категория на русском языке</label>
    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="категория" name="ru_name" value="{{ $category->ru_name }}">
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Category at english</label>
    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="category" name="en_name" value="{{ $category->en_name }}">
  </div>

 
 <div class="panel panel-default" style="width: 48%; display: inline-block;">
  <div class="panel-heading">
    <h3 class="panel-title">Изображение категории</h3>
  </div>
  <div class="panel-body">
       
             <img src="{{ url($category->image) }}" style="width: 30%; padding-bottom: 3px; height: 80px;" id="current_category_image">

  </div>
  <label for="exampleInputFile">Изменить изображение категории</label>
    <input name="image" type="file" class="form-control" id="category_image" />
</div>
<div class="clear_float"></div>




  <button type="submit" class="btn btn-primary">Сохранить</button>
</form>

@stop