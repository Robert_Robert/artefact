@extends('admin.admin-layout')

       @section('content-header')
        <h1>Статистика покупки товаров за последних 30 дней</h1>
       @stop


        @section('content')

      <div class="panel panel-default">
       <div class="panel-body">
         <h4>Для изменения выберете дату</h4>
        <form action='{{url("admin/date_statistic")}}' method="GET">
         <div class="input-group" style="width: 40%; float: left;">
          <span class="input-group-addon" id="basic-addon1">Начальная дата</span>
           <input type="text" name="start_date" class="form-control" placeholder="yyyy-mm-dd" aria-describedby="basic-addon1" id="start_date">
         </div>



         <div class="input-group"  style="width: 40%; float: right;">
          <input type="text" name="end_date" class="form-control" placeholder="yyyy-mm-dd" aria-describedby="basic-addon1" id="end_date">
          <span class="input-group-addon" id="basic-addon2">Конечная дата</span>
        </div>
         <input  class="btn btn-primary" style="width: 20%;" type="submit" name="calculate" value="Узнать">
        <div style='clear:both;'></div>
      </form>






<div class="box box-default" style="margin-top: 10px;">
   <div class="box-body">
     <div id="sales-chart"></div>
     <script>
               //DONUT CHART
               var donut = new Morris.Donut({
                 element: 'sales-chart',
                 resize: true,
                 colors: ["#0000EE", "#FF0000", "#CD00CD"],
                 data: [
                   {label: "Декор", value: {{$decor}} },
                   {label: "Освещение", value: {{$iluminate}} },
                   {label: "Мебель", value: {{$furniture}} }
                 ],
                 hideHover: 'auto'
               });
     </script>
   </div><!-- /.box-body -->
</div><!-- /.box -->





       </div>
     </div>

        @stop
