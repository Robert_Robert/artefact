@extends('admin.admin-layout')

       @section('content-header')
        <h1>Статистика покупки товаров из категорий </h1>
       @stop


        @section('content')
        <div id="statistic-area" style="height: 250px;"></div>
        <script src="{{ asset('resources/assets/admin/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
        <!-- Morris chart js -->
        <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        <script src="{{url('resources/assets/admin/plugins/morris/morris.min.js')}}"></script>
        <script>
        // Morris.Area({
        // element: 'statistic-area',
        // data: [
        // { y: '{{$cur_date}}', a: {{$order_decor_cur_mounth}}, b: {{$order_furniture_cur_mounth}}, c: {{$order_iluminate_cur_mounth}} },
        // { y: '{{$date_prev_mounth}}', a: {{$order_decor_prev_mounth}},  b: {{$order_furniture_cur_mounth}}, c: {{$order_iluminate_prev_mounth}} },
        // { y: '{{$date_two_prev_mounth}}', a: {{$order_decor_prev_two_mounth}},  b: {{$order_furniture_prev_two_mounth}}, c: {{$order_iluminate_prev_two_mounth}} },
        // { y: '{{$date_three_prev_mounth}}', a: {{$order_decor_prev_three_mounth}},  b: {{$order_furniture_prev_three_mounth}}, c: {{$order_iluminate_prev_three_mounth}} },
        // { y: '{{$date_four_prev_mounth}}', a: {{$order_decor_prev_four_mounth}},  b: {{$order_furniture_prev_four_mounth}}, c: {{$order_iluminate_prev_four_mounth}} },
        // { y: '{{$date_five_prev_mounth}}', a: {{$order_decor_prev_five_mounth}},  b: {{$order_furniture_prev_five_mounth}}, c: {{$order_iluminate_prev_five_mounth}} },
        // { y: '{{$date_six_prev_mounth}}', a: {{$order_decor_prev_five_mounth}}, b: {{$order_furniture_prev_six_mounth}}, c: {{$order_iluminate_prev_five_mounth}} }
        // ],
        // xkey: 'y',
        // ykeys: ['a', 'b', 'c'],
        // labels: ['Декор', 'Мебли', 'Освещение']
        // });



        Morris.Bar({
  element: 'statistic-area',
   data: [
        { y: '{{$cur_date}}', a: {{$order_decor_cur_mounth}}, b: {{$order_furniture_cur_mounth}}, c: {{$order_iluminate_cur_mounth}} },
        { y: '{{$date_prev_mounth}}', a: {{$order_decor_prev_mounth}},  b: {{$order_furniture_prev_mounth}}, c: {{$order_iluminate_prev_mounth}} },
        { y: '{{$date_two_prev_mounth}}', a: {{$order_decor_prev_two_mounth}},  b: {{$order_furniture_prev_two_mounth}}, c: {{$order_iluminate_prev_two_mounth}} },
        { y: '{{$date_three_prev_mounth}}', a: {{$order_decor_prev_three_mounth}},  b: {{$order_furniture_prev_three_mounth}}, c: {{$order_iluminate_prev_three_mounth}} },
        { y: '{{$date_four_prev_mounth}}', a: {{$order_decor_prev_four_mounth}},  b: {{$order_furniture_prev_four_mounth}}, c: {{$order_iluminate_prev_four_mounth}} },
        { y: '{{$date_five_prev_mounth}}', a: {{$order_decor_prev_five_mounth}},  b: {{$order_furniture_prev_five_mounth}}, c: {{$order_iluminate_prev_five_mounth}} },
        { y: '{{$date_six_prev_mounth}}', a: {{$order_decor_prev_five_mounth}}, b: {{$order_furniture_prev_six_mounth}}, c: {{$order_iluminate_prev_five_mounth}} }
        ],
  xkey: 'y',
  ykeys: ['a', 'b', 'c'],
   labels: ['Декор', 'Мебли', 'Освещение']
});
        </script>

        @stop
