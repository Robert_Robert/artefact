@extends('admin.admin-layout')

@section('content-header')
	<h1>Список установленых скидок</h1>
@stop


@section('content')
<h3 style="color: green;">{{ session('message') }}</h3>
@foreach ($discounts as $discount)
    <p class="bg-info admin-article">{{ \App\Models\Shopper_discount::getMailName($discount->shopper_id) }}<a href="{{ url('admin/delete-discount/'.$discount->id) }}" style="float: right;"><span class="glyphicon glyphicon-remove admin-article-glyph" ></a><a <a href="{{ url('admin/edit_dicount_form/'.$discount->id) }}" style="float: right;"><span class="glyphicon glyphicon-pencil admin-article-glyph" ></a></p>
@endforeach

<?php echo $discounts->links('vendor.pagination.bootstrap-4'); ?>

@stop
