@extends('admin.admin-layout')

@section('content-header')
	<h1>Добавление категории</h1>
@stop


@section('content')
<h2>Добавьте категорию на трех языках</h2>
<h3 style="color: green;">{{ session('message') }}</h3>
	<form action="{{ url('/admin/add-category') }}" method="POST" id="add_category" enctype="multipart/form-data">
   {{ csrf_field() }}
  <div class="form-group">
    <label for="exampleInputEmail1">Категорія на українській мові</label>
    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="категорія" name="ua_name" >
  </div>
 <div class="form-group">
    <label for="exampleInputEmail1">Категория на русском языке</label>
    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="категория" name="ru_name">
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Category at english</label>
    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="category" name="en_name">
  </div>

<pre style="width: 48%; display: inline-block;">
  <div class="form-group" id="add_image">
  <!-- <img src="{{ asset('resources/assets/admin/dist/img/plus.png') }}" style="float: right; cursor: pointer;" id="add_img"> -->
    <label for="exampleInputFile">Изображение категории</label>
    <input name="image" type="file" class="form-control"/>
  </div>
 </pre>
 <div class="clear_float"></div>

  <button type="submit" class="btn btn-primary">Добавить</button>
</form>
@stop