@extends('admin.admin-layout')

@section('content-header')
	<h1>Список товаров</h1>
@stop


@section('content')

<h3 style="color: green;">{{ session('message') }}</h3>
@foreach ($products as $product)

    <p class="bg-info admin-article">{{ $product->ru_name }}<a href="{{ url('admin/delete_product/'.$product->id) }}" style="float: right;"><span class="glyphicon glyphicon-remove admin-article-glyph" ></a><a <a href="{{ url('admin/edit_product/'.$product->id) }}" style="float: right;"><span class="glyphicon glyphicon-pencil admin-article-glyph" ></a></p>
@endforeach

<?php 


echo $products->links('vendor.pagination.bootstrap-4');


 ?>


@stop