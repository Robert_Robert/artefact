@extends('admin.admin-layout')

@section('content-header')
	<h1>Добавление категории</h1>
@stop


@section('content')
<h2>Добавьте под-категорию на трех языках</h2>
<h3 style="color: green;">{{ session('message') }}</h3>
	<form action="{{ url('/admin/add-sub-category') }}" method="POST" id="add_category">
   {{ csrf_field() }}

 @if (count($categoryes) > 0)
    <div class="form-group">
    <label for="exampleInputEmail1">Выберете категорию</label>
    <select class="form-control" name="main_category" >
      @foreach ($categoryes as $category)
        <option value="{{ $category->id }}">{{ $category->ru_name }}</option>
      @endforeach
    </select>
  </div>
 @endif

  <div class="form-group">
    <label for="exampleInputEmail1">Під-категорія на українській мові</label>
    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="категорія" name="ua_name" >
  </div>
 <div class="form-group">
    <label for="exampleInputEmail1">Под-категория на русском языке</label>
    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="категория" name="ru_name">
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Sub-category at english</label>
    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="category" name="en_name">
  </div>
  <button type="submit" class="btn btn-primary">Добавить</button>
</form>
@stop