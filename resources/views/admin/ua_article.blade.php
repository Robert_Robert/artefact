@extends('admin.admin-layout')

@section('content-header')
	<h1>Добавление категории</h1>
@stop


@section('content')
<h2>Добавити статтю на українській мові</h2>
<h3 style="color: green;">{{ session('message') }}</h3>
@if (count($errors) > 0)
  <div class="alert alert-danger">
    <ul>
      @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
@endif
	<form action="{{ url('/admin/blog/add-ua-article') }}" method="POST" id="add_ua_article" enctype="multipart/form-data">
   {{ csrf_field() }}
  <div class="form-group">
    <label for="exampleInputEmail1">Назва статті</label>
    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="назва" name="ua_name" >
  </div>
 <div class="form-group">
    <label for="exampleInputEmail1">Текст статті</label>
    <textarea class="form-control"  name="ua_text"></textarea>
 </div>

 <pre style="width: 48%;">
  <div class="form-group" >
    <label for="exampleInputFile">Добавьте обложку статьи</label>
    <input name="cover" type="file" class="form-control"/>
  </div>
 </pre>

  <button type="submit" class="btn btn-primary">Добавити</button>


</form>
<iframe id="form_target" name="form_target" style="display:none"></iframe>

            <form id="my_form" action="{{ url('admin/save_image') }}" target="form_target" method="post" enctype="multipart/form-data" style="width:0px;height:0;overflow:hidden">
            {{ csrf_field() }}
            <input name="image" type="file"  class="s_img" onchange="$('#my_form').submit();this.value='';">
            </form>
@stop