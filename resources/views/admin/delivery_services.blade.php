@extends('admin.admin-layout')

@section('content-header')
	<h1>Службы доставки</h1>
@stop


@section('content')

<h3 style="color: green;">{{ session('message') }}</h3>
@if (count($errors) > 0)
  <div class="alert alert-danger">
    <ul>
      @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
@endif

<form action="{{url('/admin/add_delivery_sevice')}}" method="POST" style="margin: 20px;">
{{ csrf_field() }}  
  <input type="text" name="ru_name" placeholder="на русском" class="form-control cell">
  <input type="text" name="ua_name" placeholder="на українській" class="form-control cell" >
  <input type="text" name="en_name" placeholder="at english" class="form-control cell" >
  <button type="submit" class="btn btn-primary "  style="margin-left: 5px; height: 30px; margin-bottom: 5px;"">Добавить</button>
</form>


@foreach ($services as $service)
    <p class="bg-info admin-article">{{$service->ru_name}}<a href="{{ url('admin/delete-service/'.$service->id) }}" style="float: right;"><span class="glyphicon glyphicon-remove admin-article-glyph" ></a></p>
@endforeach
              

@stop

 