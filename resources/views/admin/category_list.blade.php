@extends('admin.admin-layout')

@section('content-header')
	<h1>Список категорий</h1>
@stop


@section('content')

<h3 style="color: green;">{{ session('message') }}</h3>
@foreach ($categoryes as $category)

    <p class="bg-info admin-article">{{ $category->ru_name }}<a href="{{ url('admin/delete_category/'.$category->id) }}" style="float: right;"><span class="glyphicon glyphicon-remove admin-article-glyph" ></a><a <a href="{{ url('admin/edit_category/'.$category->id) }}" style="float: right;"><span class="glyphicon glyphicon-pencil admin-article-glyph" ></a></p>
@endforeach



@stop