@extends('admin.admin-layout')

@section('content-header')
	<h1>Добавление категории</h1>
@stop


@section('content')
<h2>Добавить статью на русском</h2>
@if (count($errors) > 0)
  <div class="alert alert-danger">
    <ul>
      @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
@endif
<h3 style="color: green;">{{ session('message') }}</h3>
	<form action="{{ url('/admin/blog/add-ru-article') }}" method="POST" id="add_ru_article" enctype="multipart/form-data">
   {{ csrf_field() }}
  <div class="form-group">
    <label for="exampleInputEmail1">Название статьи</label>
    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="название" name="ru_name" >
  </div>
 <div class="form-group">
    <label for="exampleInputEmail1">Текст статьи</label>
    <textarea class="form-control"  name="ru_text"></textarea>
  </div>
  <pre style="width: 48%;">
  <div class="form-group" >
    <label for="exampleInputFile">Добавьте обложку статьи</label>
    <input name="cover" type="file" class="form-control"/>
  </div>
 </pre>
  
  <button type="submit" class="btn btn-primary">Добавить</button>
</form>
<iframe id="form_target" name="form_target" style="display:none"></iframe>

            <form id="my_form" action="{{ url('admin/save_image') }}" target="form_target" method="post" enctype="multipart/form-data" style="width:0px;height:0;overflow:hidden">
            {{ csrf_field() }}
            <input name="image" type="file"  class="s_img" onchange="$('#my_form').submit();this.value='';">
            </form>
@stop