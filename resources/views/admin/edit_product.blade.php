@extends('admin.admin-layout')

@section('content-header')
	<h1>Редактирование товара</h1>
@stop



@section('content')
@if (count($errors) > 0)
  <div class="alert alert-danger">
    <ul>
      @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
@endif
	<form action="{{ url('/admin/save_change_product/'.$product->id) }}" method="POST" enctype="multipart/form-data" >
  {{ csrf_field() }}
  <div class="form-group">
    <label for="exampleInputEmail1">Назва товару</label>
    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Назва товару" name="ua_name" value="{{ $product->ua_name }}">
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Название товара</label>
    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Название товара" name="ru_name" value="{{ $product->ru_name }}">
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Product name</label>
    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Product name" name="en_name" value="{{ $product->en_name }}">
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Опис товару</label>
    <textarea class="form-control" name="ua_description">{{ $product->ua_description }}</textarea>
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Описание товара</label>
     <textarea class="form-control" name="ru_description">{{ $product->ru_description }}</textarea>
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Product description </label>
     <textarea class="form-control" name="en_description">{{ $product->en_description }}</textarea>
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Цена в доларах США</label>
    <input type="text" class="form-control"  placeholder="price" name="price" value="{{ $product->price }}">
  </div>
   <div class="form-group">
    <label for="exampleInputEmail1">Количество товара</label>
    <input type="text" class="form-control"  placeholder="количество" name="quantity" value="{{ $product->main_quantity }}">
  </div>
  @if (count($main_categoryes) > 0)
    <div class="form-group" style="width: 48%; display: inline-block;">
    <label for="exampleInputEmail1">Выберете категорию</label>
    <select class="form-control" name="main_category" id="main_category"  >
     <option value="">Выберете категорию</option>

      @foreach ($main_categoryes as $category)
               <option value="{{ $category->id }}" @if ($category->id == $product->category) selected @endif>{{ $category->ru_name }}</option>
      @endforeach
    </select>
  </div>
 @endif
  
    <div class="form-group" style="width: 48%; display: inline-block; float: right;">
    <label for="exampleInputEmail1">Выберете под-категорию</label>
    <select class="form-control" name="sub_category" id="sub_category" >
   @foreach ($sub_category as $category)
               <option value="{{ $category->id }}" @if ($category->id == $product->sub_category) selected @endif>{{ $category->ru_name }}</option>
      @endforeach
    </select>
  </div>

  <div style="clear: both; width: 100%;"></div>

<pre style="width: 48%; display: inline-block;" id="add_image_block">
  <div class="form-group" id="add_image">
  <img src="{{ asset('resources/assets/admin/dist/img/plus.png') }}" style="float: right; cursor: pointer;" id="add_img">
    <label for="exampleInputFile" style="font-size: 127%;">Добавить новые фотографии товара</label>
    <input name="image[]" type="file" class="form-control"/>
  </div>
 </pre>

 <div id="add_model_block">
  <div class="form-group" id="add_schema">
  <!-- <img src="{{ asset('resources/assets/admin/dist/img/plus.png') }}" style="float: right; cursor: pointer;" id="add_sch"> -->
    <label for="exampleInputFile" style="margin-top: 7%;">Схема товара(pdf-файл)</label>
    <input name="schema[]" type="file" class="form-control"/>
    <label for="exampleInputFile" style="margin-top: 7%;">3D модель товара</label>
    <input name="model" type="file" class="form-control"/>
  </div>
 </div>

 <br>

 
 <div class="panel panel-default" style="width: 48%; display: inline-block; float: left;" id="pictures_of_product">
  <div class="panel-heading">
    <h3 class="panel-title">Фотографии товара</h3>
  </div>
  <div class="panel-body">
       @foreach ($images as $image)
             <img src="{{ url($image->image) }}" style="width: 30%; padding-bottom: 3px; height: 80px;"><a data-model="{{ $image->id }}"  style="cursor: pointer;" class="del_image"><span class="glyphicon glyphicon-remove"></span></a>
          
        

      @endforeach
  </div>
</div>


 <div class="panel panel-default" style="width: 48%; display: inline-block; float: right; margin-bottom: 10px;" id="product_schema_blocl">
  <div class="panel-heading">
    <h3 class="panel-title">Схема товара</h3>
  </div>
  <div class="panel-body">
      @foreach ($schemas as $schema)
             <a href="{{ url($schema->image) }}">Просмотреть схему</a><a data-model="{{ $schema->id }}"  style="cursor: pointer;" class="del_schema"><span class="glyphicon glyphicon-remove" style="top: 3px; margin-left: 4px;"></span></a>
      @endforeach
  </div>
</div>

<div class="panel panel-default" style="width: 48%; display: inline-block; float: right;" >
  <div class="panel-heading">
    <h3 class="panel-title">3D модель</h3>
  </div>
  <div class="panel-body">
      @foreach ($models as $model)
             <a href="{{ url($model->model) }}">Просмотреть модель</a><a data-model="{{ $model->id }}"  style="cursor: pointer;" class="del_model"><span class="glyphicon glyphicon-remove" style="top: 3px; margin-left: 4px;"></span></a>
      @endforeach
  </div>
</div>

 <br>
  <button type="submit" class="btn btn-default">Добавить</button>
</form>
<iframe id="form_target" name="form_target" style="display:none"></iframe>

            <form id="my_form" action="{{ url('admin/save_image') }}" target="form_target" method="post" enctype="multipart/form-data" style="width:0px;height:0;overflow:hidden">
            {{ csrf_field() }}
            <input name="image" type="file"  class="s_img" onchange="$('#my_form').submit();this.value='';">
            </form>

@stop

