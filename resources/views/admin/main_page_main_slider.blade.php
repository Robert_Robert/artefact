@extends('admin.admin-layout')

@section('content-header')
  <h1>Управление слайдами</h1>
@stop



@section('content')
<h3 style="color: green;">{{ session('message') }}</h3>
 <div class="row">
   @foreach (\App\Models\Main_page_main_slider::get_all_slides() as $image)
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="{{ url($image->image) }}" alt="..." class="admin_slide" >
      <div class="caption">
        <h3>Текст слайда</h3>
        <form action="{{ url('admin/save_main_slide/'.$image->id) }}" method="POST"  name="update_form">
        {{ csrf_field() }}
            <input type='text' value="{{ $image->ua_description }} " class="form-control new-description" placeholder="надпись на украинском" name="title_slide_ua" >
            <input type='text' value="{{ $image->ru_description }} " class="form-control new-description" placeholder="надпись на русском" name="title_slide_ru">
            <input type='text' value="{{ $image->en_description }} " class="form-control new-description" placeholder="надпись на английском" name="title_slide_en">
        <p><button class="btn btn-primary" role="button" data-id="{{ $image->id }}" type="submit">Сохранить</button> <a href="#" class="btn btn-default btn-danger delete-slide" role="button" data-id="{{ $image->id }}">Удалить</a></p>
        </form>
      </div>
    </div>
  </div>
  @endforeach

</div>

  <form action="{{ url('admin/save_main_slide') }}" method="POST" enctype="multipart/form-data" id="main_slider" >
  {{ csrf_field() }}

<div class="panel panel-default" style="width: 50%; margin-right: auto; margin-left: auto;">
  <div class="panel-heading">Добавить новый слайд</div>
  <div class="panel-body">
    <input name="image" type="file" class="form-control" id="image" />
    <input type="text" class="form-control title_slide" placeholder="надпись на украинском" name="title_slide_ua" >
    <input type="text" class="form-control title_slide" placeholder="надпись на русском" name="title_slide_ru" >
    <input type="text" class="form-control title_slide" placeholder="надпись на английском" name="title_slide_en" >
  </div>
</div>

 
 <br>
  <button type="submit" class="btn btn-default">Добавить</button>
</form>


@stop

