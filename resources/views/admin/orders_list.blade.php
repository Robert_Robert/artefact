@extends('admin.admin-layout')

@section('content-header')
	<h1>История заказов</h1>
@stop


@section('content')

<h3 style="color: green;">{{ session('message') }}</h3>


              <table class="table table-striped">
                <tbody><tr>
                  <th style="width: 10px">#</th>
                  <th>Статус заказа</th>
                  <th>EMAIL покупателя</th>
                  <th >Сумма заказа</th>
                  <th >Сообщение</th>
                  <th>Дата</th>

                </tr>
                @foreach($orders as $order)
                <tr>
                  <td><a href="{{url('admin/order-details/'.$order->id)}}">{{$order->id}}</a></td>
                  <td>
                  @if($order->status == 'оплачено')
                  <span class="badge bg-green">{{$order->status}}</span></td>
                  @else
                  <span class="badge bg-red">{{$order->status}}</span></td>
                  @endif
                  <td>
                   {{$order->user_mail}}
                  </td>
                  <td><span class="badge bg-light-blue">{{$order->order_price}}$</span></td>
                  <td>{{$order->message}}</td>
                  <td><span class="badge bg-yellow">{{$order->created_at}}
             
                  </span></td>
                </tr>
                @endforeach
              </tbody></table>
<?php echo $orders->links('vendor.pagination.bootstrap-4'); ?>

@stop

 