@extends('admin.admin-layout')

@section('content-header')
  <h1>Редактирование товара</h1>
@stop



@section('content')
<h3 style="color: green;">{{ session('message') }}</h3>
  <form action="{{ url('/admin/slider') }}" method="POST" enctype="multipart/form-data" >
  {{ csrf_field() }}


   <div class="panel panel-default" style="width: 100%; ">
  <div class="panel-heading">
    <h3 class="panel-title">Фотографии</h3>
  </div>
  <div class="panel-body">
       @foreach ($images as $image)
             <img src="{{ url($image->image) }}" style="width: 30%; padding-bottom: 3px; height: 80px;"><a data-model="{{ $image->id }}"  style="cursor: pointer;" class="del_slide"><span class="glyphicon glyphicon-remove"></span></a>
      @endforeach
  </div>
  </div>
  

<pre style="width: 100%;">
  <div class="form-group" id="add_image">
  <img src="{{ asset('resources/assets/admin/dist/img/plus.png') }}" style="float: right; cursor: pointer;" id="add_img">
    <label for="exampleInputFile">Добавить новые фотографии товара</label>
    <input name="image[]" type="file" class="form-control"/>
  </div>
</pre>

 
 <br>
  <button type="submit" class="btn btn-default">Добавить</button>
</form>


@stop

