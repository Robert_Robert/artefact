


<div class="pagination">
            <div class="pagination-outer">

            @if ($paginator->onFirstPage())
       
            <span class="prev-pag"></span>
        
            @else
                <a href="{{ $paginator->previousPageUrl() }}">
            <span class="prev-pag"></span>
                </a>
            @endif



             <!-- Pagination Elements -->
    @foreach ($elements as $element)
        <!-- "Three Dots" Separator -->
        <ul id="pag-ul">
        @if (is_string($element))
            <li class="disabled"><span>{{ $element }}</span></li>
        @endif

        <!-- Array Of Links -->
        @if (is_array($element))

            @foreach ($element as $page => $url)
                @if ($page == $paginator->currentPage())
                    <li class="pag_li"><span class="active_li"><!-- {{ $page }} -->@if( $page < 10 )
                    0{{ $page }}
                    @else
                    {{ $page }}
                    @endif
                    </span></li>
                @else
                    <li class="pag-li"><a href="{{ $url }}">@if( $page < 10 )
                    0{{ $page }}
                    @else
                    {{ $page }}
                    @endif</a></li>
                @endif
            @endforeach
        @endif
        </ul>
    @endforeach
   <!-- Next Page Link -->
    @if ($paginator->hasMorePages())
     <a href="{{ $paginator->nextPageUrl() }}">
            <span class="next-pag"></span>
     </a>
    @else
         
            <span class="next-pag"></span>
        
    @endif 
               
            </div>
</div>
