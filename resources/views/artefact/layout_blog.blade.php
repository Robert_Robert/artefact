<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, height=device-height initial-scale=1.0">

    <title>@if(isset($article))
                    @if(isset($article->ua_name))
                        {{ $article->ua_name }}
                    @elseif(isset($article->ru_name))
                        {{ $article->ru_name }}   
                    @elseif(isset($article->en_name))
                        {{ $article->en_name }}
                    @endif
                @else
             Блог
            @endif    
    </title>
    <link rel="stylesheet" href="{{ asset('resources/assets/artefact/css/reset.css') }}">
    <link rel="stylesheet" href="{{ asset('resources/assets/artefact/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('resources/assets/artefact/css/jquery.fullPage.css') }}">
    <link rel="stylesheet" href="{{ asset('resources/assets/artefact/fonts/fonts.css') }}">
    <link rel="stylesheet" href="{{ asset('resources/assets/artefact/css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('resources/assets/artefact/css/owl.theme.css') }}">
    <link rel="stylesheet" href="{{ asset('resources/assets/artefact/css/owl.transitions.css') }}">
    <script src="https://code.jquery.com/jquery-1.7.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="{{ asset('resources/assets/artefact/js/jquery.fullPage.js') }}"></script>
    <script src="{{ asset('resources/assets/artefact/js/main.js') }}"></script>
    <script src="{{ asset('resources/assets/artefact/js/owl.carousel.js') }}"></script>
</head>
<body>
<div class="main-wrapper">
    
    @include('artefact.header')

    @yield('content')
   
    @include('artefact.footer')

</div>

</body>
</html>