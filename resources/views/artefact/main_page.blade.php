<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, height=device-height initial-scale=1.0">
    <title>Artefact</title>
    <link rel="stylesheet" href="{{ asset('resources/assets/artefact/css/reset.css') }}">
    <link rel="stylesheet" href="{{ asset('resources/assets/artefact/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('resources/assets/artefact/css/jquery.fullPage.css') }}">
    <link rel="stylesheet" href="{{ asset('resources/assets/artefact/fonts/fonts.css') }}">
    <link rel="stylesheet" href="{{ asset('resources/assets/artefact/css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('resources/assets/artefact/css/owl.theme.css') }}">
    <link rel="stylesheet" href="{{ asset('resources/assets/artefact/css/owl.transitions.css') }}">
    <script src="https://code.jquery.com/jquery-1.7.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="{{ asset('resources/assets/artefact/js/jquery.fullPage.js') }}"></script>
    <script src="{{ asset('resources/assets/artefact/js/main.js') }}"></script>
    <script src="{{ asset('resources/assets/artefact/js/owl.carousel.js') }}"></script>
</head>
<body>
<div class="main-wrapper">
@include('artefact.header')
  <div class="vertical-nav">
   <ul id="menu">
	<li data-menuanchor="firstPage"><a href="#firstPage">01</a></li>
	<li data-menuanchor="secondPage"><a href="#secondPage">02</a></li>
	<li data-menuanchor="3rdPage"><a href="#3rdPage">03</a></li>
	<li data-menuanchor="4thpage"><a href="#4thpage">04</a></li>
	<li data-menuanchor="5thpage"><a href="#5thpage">05</a></li>
</ul>
</div>

<div id="fullpage">
	<div class="section" id="section0">
    @foreach($main_slides as $slide)
      <div class="slide" style="background-image: url('{{  asset($slide->image)  }}');">
            <div class="intro">
              <h1> @if (config('app.locale') == 'ru')
                                    {{ $slide->ru_description }}
                                @elseif (config('app.locale') == 'ua')
                                    {{ $slide->ua_description }}
                                @else
                                    {{ $slide->en_description}}
                                @endif</h1>
            </div>
      </div>
    @endforeach
	</div>
	<div class="section" id="section1">
<div id="tabs" class="list-flex">
   <ul id="ul-tab" >
    @foreach($main_categoryes as $category)
        <li class="dec-li @if($category->id == 1)
          active-li 
        @endif
         "><a href="#tabs-{{$category->id}}"> @if (config('app.locale') == 'ru')
                                    {{ $category->ru_name }}
                                @elseif (config('app.locale') == 'ua')
                                    {{ $category->ua_name }}
                                @else
                                    {{ $category->en_name}}
                                @endif</a></li>
    <li>\</li>
    @endforeach
    <li class="dec-li"><a href="#tabs-4">{{trans('messages.news')}}</a></li>
  </ul>


 @foreach($main_categoryes as $category)
       
    <div id="tabs-{{$category->id}}">
      <div class="tab-title">
        <h5>                   @if (config('app.locale') == 'ru')
                                    {{ $category->ru_name }}
                                @elseif (config('app.locale') == 'ua')
                                    {{ $category->ua_name }}
                                @else
                                    {{ $category->en_name}}
                                @endif
                                </h5>
        <span></span>
      </div>
      
      @if($category->id == 1)
      <div id="owl-demo">
      @else
      <div id="owl-demo{{$category->id}}">
      @endif

      @foreach(App\Models\Product::getLatestMainPage($category->id) as $product)
        <a href="{{url('product/'.$product->id)}}" >
          <div class="item">@foreach(App\Models\Product::get_first_image($product->id) as $image)<img src="{{url($image->image) }}" alt="Owl Image" >@endforeach
            <div class="owl-hov">
               <div class="out-hov">
                 <div class="hov-title">
                    <h6>@if (config('app.locale') == 'ru')
                                    {{ $product->ru_name }}
                                @elseif (config('app.locale') == 'ua')
                                    {{ $product->ua_name }}
                                @else
                                    {{ $product->en_name}}
                                @endif</h6>
                 </div>
               </div>
            </div>
          </div>
        </a>  
      @endforeach  
      </div>
    </div>
  @endforeach

  <div id="tabs-4">
    <div class="tab-title">
        <h5>{{trans('messages.news')}}</h5>
        <span></span>
    </div>
    <div id="owl-demo4">
   


 @foreach (App\Models\Product::getLatestForMain() as $product) 
   <a href="{{ url('product/'.$product->id) }}">
  <div class="item"> @foreach(App\Models\Product::get_first_image($product->id) as $image)
                                            <img src="{{ url($image->image) }}" alt="Owl Image">
                    @endforeach
    <div class="owl-hov">
          <div class="out-hov">
          <div class="hov-title">
          <h6> @if (config('app.locale') == 'ru')
                                         {{ $product->ru_name }}  
                                    @elseif (config('app.locale') == 'ua')
                                         {{ $product->ua_name }} 
                                    @else
                                         {{ $product->en_name }}  
                                    @endif</h6>
          </div>
      </div>
  </div>
  </div>
  </a>
 @endforeach



</div>
  </div>
</div>

	</div>
	<div class="section" id="section2">
	    <div class="about">
	        <div class="left-about">
	            <span><i>{{trans('messages.about')}}</i></span>
	            <span>Artefact Design</span>
	            <span class="sep"></span>
	            <p>Фабульный каркас, в том числе, изменяем. Литургическая драма выстраивает незначительный реализм.</p>
	            <div class="about-button-area">
	                <ul>
	                    <li><a href="">{{trans('messages.who')}}</a></li>
	                    <li><a href="">{{trans('messages.what')}}</a></li>
	                </ul>
	            </div>
	        </div>
	        <div class="right-about"></div>
	    </div>
	</div>
	<div class="section" id="section3">
            <div class="title-inst">
                <span>{{trans('messages.monitoring')}}<a href="">#artefact_design</a></span>
                <span class="sep"></span>
                <p>Ганимед существенно представляет собой экспериментальный</p>
            </div>
        <div id="owl-demo5">
   

        @foreach($slides as $slide)
           <a href="" >
            <div class="item">
              <div class="out-img-inst">
                 <img src="{{ asset($slide->image) }}" alt="Owl Image">
                  <div class="owl-hov">
                      <div class="out-hov">
                         <div class="hov-title">
                         </div>
                      </div>
                  </div>
              </div>
            </div>
           </a>
        @endforeach
       
        </div>

	</div>
	<div class="section" id="section4">
		<div class="services">
		    <div class="left-service">
		        <div class="out-serv">
		            <div class="inside-serv">
		                <span>{{trans('messages.go_to')}}<a href="{{url('/shop')}}">{{trans('messages.shopp')}}</a></span>
		                <span class="sep"></span>
		                <p>Коллективное бессознательное противоречиво представляет собой эскапизм.</p>
		            </div>
		            <div class="btn-shop">
		                <a href="">{{trans('messages.buy')}}</a>
		            </div>
		        </div>

		    </div>
		    <div class="right-service">
		         <div class="out-serv">
		            <div class="inside-serv">
		                <span>{{trans('messages.review')}} <a href="">{{trans('messages.services')}} </a></span>
		                <span class="sep"></span>
		                <p> Скиннер выдвинул концепцию подкрепляемого научения</p>
		            </div>
		            <div class="btn-shop">
		                <a href="">{{trans('messages.opportunity')}}</a>
		            </div>
		        </div>
		    </div>
		</div>
	</div>
</div>

@include('artefact.footer')
</div>
</body>
</html>