<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Магазин</title>
    <link rel="stylesheet" href="{{ asset('resources/assets/artefact/css/reset.css') }}">
    <link rel="stylesheet" href="{{ asset('resources/assets/artefact/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('resources/assets/artefact/css/jquery.fullPage.css') }}">
    <link rel="stylesheet" href="{{ asset('resources/assets/artefact/fonts/fonts.css') }}">
    <link rel="stylesheet" href="{{ asset('resources/assets/artefact/css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('resources/assets/artefact/css/owl.theme.css') }}">
    <link rel="stylesheet" href="{{ asset('resources/assets/artefact/css/owl.transitions.css') }}">
    <script src="https://code.jquery.com/jquery-1.7.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="{{ asset('resources/assets/artefact/js/jquery.fullPage.js') }}"></script>
    <script src="{{ asset('resources/assets/artefact/js/main.js') }}"></script>
    <script src="{{ asset('resources/assets/artefact/js/owl.carousel.js') }}"></script>
</head>
<body>
<div class="main-wrapper">
@include('artefact.header_basket')
    <main>
        <div class="outer-main">
            <div class="inner-blog-bg  bg-light">
                <div class="title-blog">
                    <h1>{{trans('messages.shop')}}</h1>
                </div>
            </div>
            <div class="list-flex main-shop-category">
                <ul>

                    @foreach($main_categoryes as $category)
                       <li class="dec-li"> @if (config('app.locale') == 'ru')
                                         {{ $category->ru_name }}
                                    @elseif (config('app.locale') == 'ua')
                                         {{ $category->ua_name }}
                                    @else
                                         {{ $category->en_name }}
                                    @endif
                         <ul class="dropdown-content @if($category->id != 2)second-li-ch  @endif ">

                            @foreach(App\Http\Controllers\CategoryController::sub_cat_list($category->id) as $sub_category)
                                    @if (config('app.locale') == 'ru')
                                         <li><a href="{{url('/category/'.$category->id.'/'.$sub_category->id)}}">{{ $sub_category->ru_name }}</a></li>
                                    @elseif (config('app.locale') == 'ua')
                                         <li><a href="{{url('/category/'.$category->id.'/'.$sub_category->id)}}">{{ $sub_category->ua_name }}</a></li>
                                    @else
                                         <li><a href="{{url('/category/'.$category->id.'/'.$sub_category->id)}}">{{ $sub_category->en_name }}</a></li>
                                    @endif
                                @endforeach


                            <li><a href="{{url('/category/'.$category->id) }}">{{trans('messages.all')}}</a></li>
                        </ul>
                    </li>
                    @if($category->id != 3)
                    <li>\</li>
                    @endif


                    @endforeach
                </ul>
            </div>
            <div class="pag-item">
            @foreach($products as $product)
                <a href="{{url('/product/'.$product->id)}}">
                    <div class="blog-col">
                         @foreach(App\Models\Product::get_first_image($product->id) as $image)
                                            <img src="{{ url($image->image) }}" alt="Owl Image">
                        @endforeach
                        <div class="owl-hov">
                            <div class="out-hov">
                                <div class="hov-title">
                                    <small>{{$product->price}}$</small>
                                    <h6> @if (config('app.locale') == 'ru')
                                        {{ $product->ru_name }}
                                    @elseif (config('app.locale') == 'ua')
                                         {{ $product->ua_name }}
                                    @else
                                         {{ $product->en_name }}
                                    @endif</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            @endforeach
            </div>

            <?php echo $products->links(); ?>
        </div>


    </main>

    @include('artefact.footer')

    </div>
</body>
</html>
