@extends('artefact.search_layout')
@section('content')
    <main>
        <div class="outer-main">
            <div class="inner-blog-bg  bg-light" style="background-image: url('{{  asset('public/category_images/category_581a250771cf7.jpg') }}')">
                <div class="title-blog">
                <h1>{{ trans('messages.search') }}</h1>
                </div>
            </div>
            <div class="list-flex sort-shop">
               

                <div class="dropdown-xs">
                    <div class="dropbtn-xs" id="drop-text2">
                        все
                    </div>
                    <i class="ar-xs"></i>
                    <div class="dropdown-xs-content">
                        <span class="act-drop2">декоративне</span>
                        <span class="act-drop2">настінне</span>
                        <span class="act-drop2">вуличне</span>
                        <span class="act-drop2">внутрішнє</span>
                        <span class="act-drop2">все</span>
                    </div>
                </div>
                <div class="dropdown-date">
                    <div class="dropbtn" id="drop-text">
                        сортувати за
                    </div>
                    <i class="ar"></i>
                    <div id="myDropdown" class="dropdown-content">
                  </a>
                    
                         <a href="{{ url('search?filter=min_price') }}"><span class="act-drop">найменшою ціною</span></a>
                         <a href="{{ url('search?filter=max_price') }}"><span class="act-drop">найбільшою ціною</span></a>
                         <a href="{{ url('search?filter=date') }}"><span class="act-drop">датою</span></a>
                      
                    </div>
                </div>
            </div>

            <div class="pag-item">
            @foreach($products as $product)
                <a href="{{ url('/product/'.$product->id) }}">
                    <div class="blog-col">
                         @foreach(App\Models\Product::get_first_image($product->id) as $image)
                                            <img src="{{ url($image->image) }}" alt="Owl Image">
                        @endforeach
                        <div class="owl-hov">
                            <div class="out-hov">
                                <div class="hov-title">
                                    <h6>@if (config('app.locale') == 'ru')
                                  {{ $product->ru_name }}
                                @elseif (config('app.locale') == 'ua')
                                  {{ $product->ua_name }}
                                @else
                                  {{ $product->en_name }}
                                @endif</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            @endforeach
            </div>

            
          <?php echo $products->links(); ?>
              


        </div>
    </main>
   @stop 
    