@extends('artefact.basket_layout')
@section('content')
    <main class="while-wall">
        <div class="outer-main lg-align bg-grey">
            <div class="basket-outer ">
                <div class="wizard">
                    <div class="wiz-col active-wiz">
                        <i>кошик</i>
                        1
                    </div>
                    <div class="wiz-line active-wiz-line"></div>
                    <div class="wiz-col active-wiz">
                        <i>дані</i>
                        2
                    </div>
                    <div class="wiz-line active-wiz-line"></div>
                    <div class="wiz-col active-wiz">
                        <i>оплата</i>
                        3
                    </div>
                    <div class="wiz-line active-wiz-line"></div>
                    <div class="wiz-col">
                        <i>фініш</i>
                        4
                    </div>
                </div>
                <div class="payment-content">
                <h3 class="basket-title">
                    оплата карткою
                </h3>
                <p>
                    Ваше замовлення буде надіслане вам поштою. Інформація про замовлення буде надіслана вам на пошту. Наш менеджер зв’яжется з вами найближчим часом.
                </p>
                 <article>здійснити оплату через <a href="{{url('pay-request')}}">liqpay</a></article>
                 @if($discount > 0)
                    <span>
                        Сума зі знижкою <em>{{$discount}}%</em>
                        <hr>
                        <i>${{$totalprice / 100 * $discount}}</i>
                    </span>
                 @else   
                    <span>
                        сума: ${{$totalprice}}
                    </span>
                 @endif   
                </div>
                <div class="basket-count">

                    В корзині  <em>{{count($products)}}</em>  товари
                </div>
                <div class="step-nav">
                    <span>
                        <i></i>
                        <a href="basket.html">назад</a>
                    </span>
                    <span>
                        <i></i>
                        <a href="payment-complete.html">продовжити</a>
                    </span>
                </div>
            </div>
        </div>
    </main>
  {{ csrf_field() }}

  @stop   