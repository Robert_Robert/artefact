<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, height=device-height initial-scale=1.0">

    <title>Контакти</title>
    <link rel="stylesheet" href="{{ asset('resources/assets/artefact/css/reset.css') }}">
    <link rel="stylesheet" href="{{ asset('resources/assets/artefact/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('resources/assets/artefact/css/jquery.fullPage.css') }}">
    <link rel="stylesheet" href="{{ asset('resources/assets/artefact/fonts/fonts.css') }}">
    <link rel="stylesheet" href="{{ asset('resources/assets/artefact/css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('resources/assets/artefact/css/owl.theme.css') }}">
    <link rel="stylesheet" href="{{ asset('resources/assets/artefact/css/owl.transitions.css') }}">
    <script src="https://code.jquery.com/jquery-1.7.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="{{ asset('resources/assets/artefact/js/jquery.fullPage.js') }}"></script>
    <script src="{{ asset('resources/assets/artefact/js/main.js') }}"></script>
    <script src="{{ asset('resources/assets/artefact/js/owl.carousel.js') }}"></script>
</head>
<body>
<div class="main-wrapper">

    @include('artefact.header_basket')

  <main>
      <div class="out-contacts">

          <div class="left-cont">    @if (count($errors) > 0)

                    <ul>
                      @foreach ($errors->all() as $error)
                        <li  style="background-color: red; color: white; font-size: 120%;">{{ $error }}</li>
                      @endforeach
                    </ul>

                @endif
              <div class="call-cont left">
                    <h6>{{trans('messages.call')}}</h6>
                    <span>+380 (99) 066 81 51</span>
              </div>
              <div class="address-cont left">
                  <h6>{{trans('messages.welcome')}}</h6>
                <span>{{trans('messages.adres')}}</span>
              </div>



              <form action="{{ url('contacts/sendmail') }}" method="POST">
               {{ csrf_field() }}
                  <h6>{{trans('messages.message')}}</h6>
                  <input type="text" name="name" placeholder="{{trans('messages.name')}}" value="{{ old('name') }}">
                  <input type="text" name="phone" id="phone-form" pattern="^([0|\+[0-9]{5,20})?([1-9][0-9]{5,20})$" placeholder="{{trans('messages.phone')}}" value="{{ old('phone') }}">
                  <input type="email" name="email" placeholder="{{trans('messages.email')}}" value="{{ old('email') }}">
                  <textarea name="content" cols="30" rows="10" placeholder="{{trans('messages.your_message')}}">{{ old('content') }}</textarea>
                  <button class="form-sub-cont" type="submit">{{trans('messages.send')}}</button>
              </form>


          </div>
          <div class="right-cont">
       <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1328.0688567833258!2d25.95125630415597!3d48.26172429132469!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x473408d4b43c34d1%3A0x37cb12d3007e5254!2z0JPQvtC70L7QstC90LAg0LLRg9C70LjRhtGPLCAxOTjQkCwg0KfQtdGA0L3RltCy0YbRliwg0KfQtdGA0L3RltCy0LXRhtGM0LrQsCDQvtCx0LvQsNGB0YLRjA!5e0!3m2!1sru!2sua!4v1474623639381" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
          </div>
      </div>
  </main>
 @include('artefact.footer')

</div>

</body>
</html>
