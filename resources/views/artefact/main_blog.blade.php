@extends('artefact.layout_blog')
@section('content')
    <main>
        <div class="blog-outer">

         @foreach ($articles as $articl)
        <a href="{{url('blog/single-article?id='.$articl->id)}}">
        <div class="blog-col">
            <img src="{{ url($articl->image) }}" alt="">
                <div class="owl-hov">
                    <div class="out-hov">
                        <div class="hov-title">
                            <h6>
                                @if (config('app.locale') == 'ru')
                                    {{ $articl->ru_name }}
                                @elseif (config('app.locale') == 'ua')
                                    {{ $articl->ua_name }}
                                @else
                                    {{ $articl->en_name }}
                                @endif</h6>
                            <div>
                            <h6>Читати</h6>
                            <i><img src="{{ url('resources/assets/artefact/images/hover-arrow.png') }}" height="9" width="5" alt=""></i>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        </a>
        @endforeach

        </div>
    
     <?php echo $articles->links(); ?>

    </main>
   @stop 
    