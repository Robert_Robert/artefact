@extends('artefact.category_layout')
@section('content')
    <main>
        <div class="outer-main">
            <div class="inner-blog-bg  bg-light" style="background-image: url('{{  url($main_cat->image) }}')">
                <div class="title-blog">
                @if (config('app.locale') == 'ru')
                                  <h1>  {{ $main_cat->ru_name }}  </h1>
                                @elseif (config('app.locale') == 'ua')
                                  <h1>  {{ $main_cat->ua_name }}  </h1>
                                @else
                                  <h1>  {{ $main_cat->en_name }}  </h1>
                                @endif
                </div>
            </div>
            <div class="list-flex sort-shop">
                <ul>
                @foreach($sub_cats as $sub)
                    <li class="dec-li @if(isset($sub_cat) && $sub->id == $sub_cat->id)
                   active-li
                    @endif"><a href="{{url('/category/'.$main_cat->id.'/'.$sub->id)}}">
                      @if (config('app.locale') == 'ru')
                                  {{ $sub->ru_name }}
                                @elseif (config('app.locale') == 'ua')
                                  {{ $sub->ua_name }}
                                @else
                                  {{ $sub->en_name }}
                                @endif
                    </a></li>
                    <li>\</li>
                @endforeach
                @if(!isset($sub_cat))
                 <li class="dec-li active-li"><a href="{{ url('/category/'.$main_cat->id) }}">{{trans('messages.all')}}</a></li>
                @else
                 <li class="dec-li"><a href="{{ url('/category/'.$main_cat->id) }}">{{trans('messages.all')}}</a></li>
                @endif
                </ul>

                <div class="dropdown-xs">
                    <div class="dropbtn-xs" id="drop-text2">
                        {{trans('messages.all')}}
                    </div>
                    <i class="ar-xs"></i>
                    <div class="dropdown-xs-content">
                        @foreach($sub_cats as $sub)
                        @if (config('app.locale') == 'ru')
                                      <span class="act-drop2">{{ $sub->ru_name }}</span>
                                  @elseif (config('app.locale') == 'ua')
                                      <span class="act-drop2">{{ $sub->ua_name }}</span>
                                  @else
                                      <span class="act-drop2">{{ $sub->en_name }}</span>
                                  @endif
                        @endforeach
                        <span class="act-drop2">{{trans('messages.all')}}</span>

                    </div>
                </div>
                <div class="dropdown-date">
                    <div class="dropbtn" id="drop-text">
                        {{trans('messages.sort')}}
                    </div>
                    <i class="ar"></i>
                    <div id="myDropdown" class="dropdown-content">
                    @if (isset($sub_cat))
                        <a href="{{ url('/category/'.$main_cat->id.'/'.$sub_cat->id.'?filter=min-price') }}"><span class="act-drop">{{trans('messages.lowest_price')}}</span></a>
                        <a href="{{ url('/category/'.$main_cat->id.'/'.$sub_cat->id.'?filter=max-price') }}"><span class="act-drop">{{trans('messages.biggest_price')}}</span></a>
                        <a href="{{ url('/category/'.$main_cat->id.'/'.$sub_cat->id.'?filter=date') }}"><span class="act-drop">{{trans('messages.date')}}</span></a>
                     @else
                         <a href="{{ url('/category/'.$main_cat->id.'?filter=min-price') }}"><span class="act-drop">{{trans('messages.lowest_price')}}</span></a>
                         <a href="{{ url('/category/'.$main_cat->id.'?filter=max-price') }}"><span class="act-drop">{{trans('messages.biggest_price')}}</span></a>
                        <a href="{{ url('/category/'.$main_cat->id.'?filter=date') }}"><span class="act-drop">{{trans('messages.date')}}</span></a>
                    @endif
                    </div>
                </div>
            </div>

            <div class="pag-item">
            @foreach($products as $product)
                <a href="{{ url('/product/'.$product->id) }}">
                    <div class="blog-col">
                         @foreach(App\Models\Product::get_first_image($product->id) as $image)
                                            <img src="{{ url($image->image) }}" alt="Owl Image">
                        @endforeach
                        <div class="owl-hov">
                            <div class="out-hov">
                                <div class="hov-title">
                                    <h6> @if (config('app.locale') == 'ru')
                                  {{ $product->ru_name }}
                                @elseif (config('app.locale') == 'ua')
                                  {{ $product->ua_name }}
                                @else
                                  {{ $product->en_name }}
                                @endif</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            @endforeach
            </div>

             <div class="pagination shop-pag" style="width: 100%">
                <div class="pagination-outer">
                    <a href="{{ url('category/'.$prev_category->id) }}">
                        <div>
                            <i></i>
                            <span>  @if (config('app.locale') == 'ru')
                                  {{ $prev_category->ru_name }}
                                @elseif (config('app.locale') == 'ua')
                                  {{ $prev_category->ua_name }}
                                @else
                                  {{ $prev_category->en_name }}
                                @endif</span>
                        </div>
                    </a>
          <?php echo $products->links('vendor.pagination.category-pagination'); ?>
               <a href="{{ url('category/'.$next_category->id) }}">
                        <div>
                            <span>
                                 @if (config('app.locale') == 'ru')
                                  {{ $next_category->ru_name }}
                                @elseif (config('app.locale') == 'ua')
                                  {{ $next_category->ua_name }}
                                @else
                                  {{ $next_category->en_name }}
                                @endif
                                </span>
                            <i></i>
                        </div>
                    </a>
                </div>
            </div>


        </div>
    </main>
   @stop
