    <header>
        <div class="head-out">
            <div class="main-logo left">
                  <a href="{{ url('/') }}"><img src="{{ url('resources/assets/artefact/images/main-logo.png') }}" width="154" height="50" alt=""></a>
            </div>
              <nav class="cd-slideshow-nav">
                <button class="cd-nav-trigger">
                    Open Nav
                    <span aria-hidden="true"></span>
                </button>
                <div class="cd-nav-items">
                    <div class="inside-nav">
                        <div class="main-nav">
                            <ul>
                                <li><a href="{{ url('/') }}">{{trans('messages.main')}}</a></li>
                                <li><a href="{{ url('/') }}">{{trans('messages.services')}}</a></li>
                                <li><a href="{{ url('/') }}">{{trans('messages.about')}}</a></li>
                                <li class="none"><a href="{{url('/shop')}}" >{{trans('messages.shop')}}</a></li>
                                <li><a href="{{ url('/blog') }}">{{trans('messages.blog')}}</a></li>
                                <li><a href="{{ url('/contacts') }}">{{trans('messages.contacts')}}</a></li>
                            </ul>
                        </div>
                        <div class="sep-small"></div>
                        <div class="sub-nav">
                            <div class="outer-menu-item">
                                <div class="sub-menu-item">
                        <span>
                       <h3><a href="{{ url('/category/1') }}">{{trans('messages.decor')}}</a></h3>

                        <div>
                          <ul>
                                @foreach(App\Http\Controllers\CategoryController::sub_cat_list(1) as $category)
                                   <li><a href="{{ url('/category/3/'.$category->id) }}">
                                    @if (config('app.locale') == 'ru')
                                         {{ $category->ru_name }}
                                    @elseif (config('app.locale') == 'ua')
                                         {{ $category->ua_name }}
                                    @else
                                         {{ $category->en_name }}
                                    @endif
                                   </a></li>
                                @endforeach
                            </ul>
                        </div>
                        </span>
                                    <span>
                             <h3><a href="{{ url('/category/3') }}">{{trans('messages.illumination')}}</a></h3>
                            <div>
                                 <ul>

                                @foreach(App\Http\Controllers\CategoryController::sub_cat_list(3) as $category)
                                   <li><a href="{{ url('/category/3/'.$category->id) }}">
                                    @if (config('app.locale') == 'ru')
                                         {{ $category->ru_name }}
                                    @elseif (config('app.locale') == 'ua')
                                         {{ $category->ua_name }}
                                    @else
                                         {{ $category->en_name }}
                                    @endif
                                   </a></li>
                                @endforeach

                                </ul>
                            </div>
                        </span>
                                    <span>
                            <h3><a href="{{ url('/category/2') }}">{{trans('messages.furniture')}}</a></h3>
                            <div>
                                <ul>
                                    @foreach(App\Http\Controllers\CategoryController::sub_cat_list(2) as $category)
                                   <li><a href="{{ url('/category/3/'.$category->id) }}">
                                    @if (config('app.locale') == 'ru')
                                         {{ $category->ru_name }}
                                    @elseif (config('app.locale') == 'ua')
                                         {{ $category->ua_name }}
                                    @else
                                         {{ $category->en_name }}
                                    @endif
                                   </a></li>
                                @endforeach
                                </ul>
                            </div>
                        </span>
                                </div>
                            </div>
                            <div class="newest-item">
                                <h5><a href="{{ url('/shop') }}">{{trans('messages.shop')}}</a></h5>
                                <div class="new-item-list">
                                    @foreach (App\Models\Product::getLatest() as $product)
                                    <a href="{{ url('product/'.$product->id) }}">
                                        <div class="item">
                                        @foreach(App\Models\Product::get_first_image($product->id) as $image)
                                            <img src="{{ url($image->image) }}" alt="Owl Image">
                                        @endforeach
                                            <div class="owl-hov">
                                                <div class="out-hov">
                                                    <div class="hov-title">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                    @endforeach

                                </div>
                            </div>
                        </div>
                        <div class="foot-menu">
                            <span>{{trans('messages.created')}} <a href="http://sandglass.com.ua/" target="_blank">sandglass</a></span>
                            <span><a href="{{ url('contacts') }}">{{trans('messages.to_order')}}</a></span>
                        </div>
                    </div>
                    <div class="sep">
                        <div class="sep-inside"></div>
                    </div>
                    <div class="contacts-nav">
                        <h5>{{trans('messages.communication')}}</h5>
                        <span class="sep-center"></span>
                        <div class="con-nav-outer">
                            <div class="cont-nav-item">
                                <h4>{{trans('messages.welcome')}}</h4>
                                <span>{{trans('messages.adres')}}
                        </span>
                            </div>
                            <div class="cont-nav-item phone-item">
                                <h4>{{trans('messages.call')}}</h4>
                                <span>+380 50 15 64 495</span>
                                <span>+380 50 15 64 495</span>
                            </div>
                        </div>
                        <div class="con-nav-outer">
                            <div class="cont-nav-item">
                                <h4>{{trans('messages.write')}}</h4>
                                <span class="late-marg">artefact@info.com</span>
                            </div>
                            <div class="btn-cont-nav">
                                <a href="{{ url('/contacts') }}">{{trans('messages.message')}}</a>
                            </div>
                        </div>
                    </div>
                    <div class="touch-us-xs">
                        <div class="foot-menu">
                            <span>{{trans('messages.created')}} <a href="http://sandglass.com.ua/" target="_blank">sandglass</a></span>
                            <span><a href="">{{trans('messages.to_order')}}</a></span>
                        </div>
                        <div class="social-nav">
                            <ul>
                                <li><a href="">Facebook</a></li>
                                <li><a href="">Instagram</a></li>
                                <li><a href="">Pinterest</a></li>
                            </ul>
                        </div>
                    </div>
                </div> <!-- .cd-nav-items -->
            </nav>
        </div>
    </header>
