@extends('artefact.layout_blog')
@section('content')
    <main>
    <div class="blog-content-outer">
        <div class="inner-blog-bg bg-on-blog" style="background-image: url('{{  asset($article->image)  }}');">
            <div class="title-blog">
            <h1>@if (config('app.locale') == 'ru')
                                    {{ $article->ru_name }}
                                @elseif (config('app.locale') == 'ua')
                                    {{ $article->ua_name }}
                                @else
                                    {{ $article->en_name }}
                                @endif</h1>
            </div>
        </div>

        <div class="blog-content">
           @if (config('app.locale') == 'ru')
                                    {!! $article->ru_text !!}
                                @elseif (config('app.locale') == 'ua')
                                    {!! $article->ua_text !!}
                                @else
                                    {!! $article->en_text !!}
                                @endif
        </div>
    </div>

    </main>
       @stop 

   