 <footer>
        <div class="foot-out">
            <div class="copy left">
                &copy; 2016 Artefact Design
            </div>
            <div class="lang">
                <div class="lang-out">
                    <ul>
                        <li><a href="{{ url('/change-language/ua') }}" class="uk-lang @if(App::getLocale() == 'ua')
                            active-lang
                            @endif">UA</a></li>
                        <li><a href="{{ url('/change-language/ru') }}" class="ru-lang @if(App::getLocale() == 'ru')
                            active-lang
                            @endif
                        ">RU</a></li>
                        <li><a href="{{ url('/change-language/en') }}" class="en-lang @if(App::getLocale() == 'en')
                            active-lang
                            @endif">EN</a></li>
                       
                    </ul>
                </div>
            </div>
            <div class="social">
                <a href="">Facebook</a>
                <a href="">Instagram</a>
                <a href="">Pinterest</a>
            </div>
        </div>
    </footer>