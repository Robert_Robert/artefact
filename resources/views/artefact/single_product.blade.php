@extends('artefact.product_layout')
@section('content')
 {{ csrf_field() }}
    <main>
        <div class="outer-main no-padd">
        <div id="owl-shop-item" class="owl-carousel owl-theme owl-inside-shop left">

               @foreach($images as $image)
                <div class="item"><img src="{{ url($image->image) }}" alt=""></div>
               @endforeach
        </div>
            <article class="item-card-info left">
                <div class="item-breadcump">
                       <p>@if (config('app.locale') == 'ru')
                                    {{ $category->ru_name }}
                                @elseif (config('app.locale') == 'ua')
                                   {{ $category->ua_name }}
                                @else
                                   {{ $category->en_name }}
                                @endif</p>
                       <p>
                           <span><a href="{{ url('product/'.$prev_product) }}">{{trans('messages.prev')}}</a></span>
                           <span><a href="{{ url('product/'.$next_product) }}">{{trans('messages.next')}}</a></span>
                       </p>
                </div>
                <h1>@if (config('app.locale') == 'ru')
                                    {{ $product->ru_name }}
                                @elseif (config('app.locale') == 'ua')
                                   {{ $product->ua_name }}
                                @else
                                   {{ $product->en_name }}
                                @endif</h1>
                <p class="item-price">${{ $product->price }}</p>

                <div class="item-stat">
                    <h3>Характеристики</h3>
                   @if (config('app.locale') == 'ru')
                                    {!! $product->ru_description !!}
                                @elseif (config('app.locale') == 'ua')
                                   {!! $product->ua_description !!}
                                @else
                                   {!! $product->en_description !!}
                                @endif
                </div>
                <div class="count-item">
                    <p>Кількість: <span>{{ $product->cur_quantity }}/{{ $product->main_quantity }}</span></p>
                </div>

                <div class="item-btn">
                    <div class="scheme-3d">
                    @if(isset($model->model))
                        <span><a href="{{ $model->model }}" download>3D</a></span>
                    @endif
                        @if(isset($schema->image))
                        <span><a href="{{ url($schema->image) }}" download>Схема</a></span>
                        @endif
                    </div>
                    <button class="form-sub-cont item-btn-buy add-to-basket" @if((isset($product))) data-product="{{$product->id}}" @endif>
                       {{ trans('messages.add_to_basket') }}
                    </button>
                    <div class="added-item">
                        <div class="buy-complete">

                            <svg version="1.1" class="svg-outer" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="90.594px" height="59.714px" viewBox="0 0 90.594 59.714" enable-background="new 0 0 90.594 59.714" xml:space="preserve">
  <polyline class="check" fill="none" stroke="#b78f69" stroke-width="5" stroke-miterlimit="10" points="1.768,23.532 34.415,56.179 88.826,1.768"/>
</svg>
                        </div>
                        Додано
                    </div>
                </div>

            </article>
        </div>
    </main>
   @stop
