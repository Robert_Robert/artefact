@extends('artefact.basket_layout')
@section('content')
    <main>


      <div class="outer-main ">
                <div class="basket-outer">
                <div class="wizard">
                    <div class="wiz-col active-wiz">
                        <i>{{ trans('messages.basket') }}</i>
                        1
                    </div>
                    <div class="wiz-line active-wiz-line"></div>
                    <div class="wiz-col">
                        <i>дані</i>
                        2
                    </div>
                    <div class="wiz-line"></div>
                    <div class="wiz-col">
                        <i>оплата</i>
                        3
                    </div>
                    <div class="wiz-line"></div>
                    <div class="wiz-col">
                        <i>фініш</i>
                        4
                    </div>
                </div>
                    <div class="basket-content">

                      @foreach($products as $product)
                       <div class="basket-item" data-product="{{$product->id}}" data-category="{{$product->category}}">
                           @foreach(App\Models\Product::get_first_image($product->id) as $item)
                            <div><img src="{{$item->image}}" width="120" height="120" alt=""></div>
                           @endforeach

                        <div class="item-desc">
                          <a href="{{url('product/'.$product->id)}}">@if (config('app.locale') == 'ru')
                                      {{ $product->ru_name }}
                                  @elseif (config('app.locale') == 'ua')
                                     {{ $product->ua_name }}
                                  @else
                                     {{ $product->en_name }}
                                  @endif</a>
                      </div>




                           <div class="one-item-price"><small  class="lol price">{{$product->price}}$</small></div>



                               <div class="number">
                                 <div class="numb-inner">
                                   <input type="text" value="1" size="100" class="quant" />
                                   <span class="plus"></span>
                                   <span class="minus"></span>
                                 </div>
                               </div>


                             <div class="item-all-price mobile-block"><p class="total-price"></p></div>

                           <span class="del-item" data-product="{{ $product->id }}"></span>

                       </div>
                       @endforeach

                    </div>
                    <div class="sum-bask">
                        Сума: $300
                    </div>
                     <input type="hidden"  id="total_order_price" name="somename" value="300$" style="display: none;"/>


                    <div class="basket-count">

                            Товарів в корзині  <em id="product_count">{{count($products)}}</em>
                        </div>
                    <div class="step-nav">
                        <span>
                            <i></i>
                            <a href="{{url('shop')}}">до магазину</a>
                        </span>
                        <span >
                            <i></i>
                                <a href="{{url('client_info')}}" id="add_basket_to_session" >продовжити</a>
                        </span>
                    </div>
            </div>

            </div>


    {{ csrf_field() }}

  @stop
