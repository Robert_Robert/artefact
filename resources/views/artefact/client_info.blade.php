@extends('artefact.basket_layout')
@section('content')
<main>


        <div class="outer-main lg-align">
            <div class="basket-outer ">
                <div class="wizard">
                    <div class="wiz-col active-wiz">
                        <i>кошик</i>
                        1
                    </div>
                    <div class="wiz-line active-wiz-line"></div>
                    <div class="wiz-col active-wiz">
                        <i>дані</i>
                        2
                    </div>
                    <div class="wiz-line active-wiz-line"></div>
                    <div class="wiz-col">
                        <i>оплата</i>
                        3
                    </div>
                    <div class="wiz-line"></div>
                    <div class="wiz-col">
                        <i>фініш</i>
                        4
                    </div>
                </div>
                <form action="{{url('add_client_info')}}"  method="post" id="signupForm">
                   {{ csrf_field() }}
            <div>
              <input type="text" name="country" placeholder="Країна" value="{{Session::get('product.client_info.country')}}">
              <input type="text" name="city" placeholder="Місто/село" value="{{Session::get('product.client_info.city')}}">
              <input type="text" name="region" placeholder="Область/регіон" value="{{Session::get('product.client_info.region')}}">
              <input type="text" name="index" placeholder="Індекс" value="{{Session::get('product.client_info.index')}}">
              <input type="text" name="adress" placeholder="Адреса" value="{{Session::get('product.client_info.adress')}}">
              <input type="email" name="email" placeholder="Ваш e-mail" value="{{Session::get('product.client_info.email')}}">
              <input type="text" name="phone" id="phone-form" pattern="^([0|\+[0-9]{5,20})?([1-9][0-9]{5,20})$" placeholder="Ваш телефон" value="{{Session::get('product.client_info.phone')}}">
           </div>

               <div>
                 <input type="text" name="name" placeholder="Ім'я" value="{{Session::get('product.client_info.name')}}">
                 <input type="text" name="surname" placeholder="Прізвище" value="{{Session::get('product.client_info.surname')}}">
                   <div class="dropdown-xs form-dropdown" style="display: block;">
                       <div class="dropbtn-xs"  style="width: 100%;">
                       <input type="text" id="droptext3" name="service_delivery" placeholder="Служба доставки *" autocomplete="off" style="cursor: pointer" value="{{Session::get('product.client_info.service_delivery')}}">
                       </div>
                       <i class="ar-xs form-ar"></i>
                       <div class="dropdown-xs-content">
                           <span class="act-drop3">Нова Пошта</span>
                           <span class="act-drop3">Міст</span>
                           <span class="act-drop3">ІнТайм</span>
                       </div>
                   </div>
               <input type="text" placeholder="Адреса відділення *" id="address_department" name="address_department" value="{{Session::get('product.client_info.address_department')}}">
                   <textarea name="area" cols="30" rows="10" placeholder="Ваше повідомлення" name="message">{{Session::get('product.client_info.message')}}</textarea>

               </div>


                </form>

                <div class="step-nav">
                    <span>
                        <i></i>
                        <a href="{{url('basket')}}">до корзини</a>
                    </span>
                    <span>
                        <i></i>
                        <a href="" id="send_client_form">продовжити</a>
                    </span>
                </div>

            </div>

        </div>
    </main>
    @stop
