$('document').ready(function(){

	// $("input[type=text]").focus(function(){
	// 	$(this).attr('value', '').css({'color':'#555'});
	// });

	$('#add_category').submit(function(ev){

		$("input[type=text]").each(function(index){


			if($(this).val() == ''){
				ev.preventDefault();

				$(this).attr('value', 'Заполните это поле!').css({'border': 'red', 'color':'red'});
			}

		});

	});

	$('#add_img').click(function(ev){
		$('#add_image').append(' <input name="image[]" type="file" class="form-control"/>');
	});

	$('#add_sld').click(function(ev){
		$('#add_slides').append('<div id="add_items" style="margin-bottom: 6px;"><input name="image[]" type="file" class="form-control"/><input type="text" class="form-control title_slide" placeholder="надпись на слайде" name="title_slide" ></div>');
	});


	$('#add_sch').click(function(ev){
		$('#add_schema').append(' <input name="schema[]" type="file" class="form-control"/>');
	});

	$('#main_category').change(function(ev){


	var main_category = $(this).val();
		var id = '', name = '';


 		 $.ajax({
	 	url: '/admin/get-sub-category',
	 	type: "POST",
	 	data :({'main_category': main_category, '_token': $('input[name=_token]').val()}),
	 	dataType: "html",

	 	success: function(data, d){
	 		$('#sub_category').empty();
	 			 var answer = JSON.parse(data);

	 				for(var i in answer){
	 					var new_object = answer[i];

	 						for(var n in new_object){
	 							var deep_object = new_object[n];

	 							if(n == 'id') id = deep_object;
	 							if(n == 'ru_name') name = deep_object;

	 						}
	 						$('#sub_category').append('<option value="'+id +'">'+ name +'</option>');

	 				}

	 		}
	 });


	});

	$('.del_image').click(function(ev){

		var model = $(this).attr('data-model'), it = $(this);

		$.ajax({
	 	url: '/admin/del_image/'+model,
	 	type: "POST",
	 	data :({'_token': $('input[name=_token]').val()}),
	 	dataType: "html",

	 	success: function(data, d){
	 			it.prev().remove();
	 			it.remove();

	 		}
	 });


	});

	$('.del_slide').click(function(ev){
		console.log(1);

		var model = $(this).attr('data-model'), it = $(this);

		$.ajax({
	 	url: '/admin/del_slide/'+model,
	 	type: "POST",
	 	data :({'_token': $('input[name=_token]').val()}),
	 	dataType: "html",

	 	success: function(data, d){
	 			it.prev().remove();
	 			it.remove();

	 		}
	 });


	});

	$('.del_schema').click(function(ev){

		var model = $(this).attr('data-model'), it = $(this);

		$.ajax({
	 	url: '/admin/del_schema/'+model,
	 	type: "POST",
	 	data :({'_token': $('input[name=_token]').val()}),
	 	dataType: "html",

	 	success: function(data, d){
	 			it.prev().remove();
	 			it.remove();

	 		}
	 });

	});

		$('.del_model').click(function(ev){

		var model = $(this).attr('data-model'), it = $(this);

		$.ajax({
	 	url: '/admin/del_model/'+model,
	 	type: "POST",
	 	data :({'_token': $('input[name=_token]').val()}),
	 	dataType: "html",

	 	success: function(data, d){
	 			it.prev().remove();
	 			it.remove();

	 		}
	 });

	});

	$('#category_image').click(function(ev){
		// $('#current_category_image').attr('src', '');

	});

	$('.new-description').change(function(ev){
		$(this).parent('p').next().find(':first-child').attr('data-description', $(this).val());
	});

	$('.delete-slide').click(function(ev){
		ev.preventDefault();

		var id = $(this).attr('data-id'), it = $(this);

		$.ajax({
	 	url: '/admin/del_slide',
	 	type: "POST",
	 	data :({'_token': $('input[name=_token]').val(), 'id': id }),
	 	dataType: "html",

	 	success: function(data, d){

	 			  it.parent('p').parent('form').parent('div').parent('div').parent('div').hide('slow', function(){
    				$(this).remove();
				 });

	 		}
	 });

	});

	$('#add_model_block').css('height', $('#add_image_block').css('height'));

	var height_of_some =  $('#product_schema_blocl').css('height');
	height_of_some = parseInt(height_of_some)*2+10;
	height_of_some = height_of_some.toString() + 'px';

	$('#pictures_of_product').css('height', height_of_some);

  $('#date_statistic').click();

	$('#display_money').css('height', $('#diagram_finance').css('height'));

	// $('#main_slider').submit(function(ev){
	// 	ev.preventDefault();
	// 	if($('input[type=file]').val() == '') alert('Выберете изображение слайда!');
	// 	var formData = new FormData();
	// 	 formData.append('title_slide', $('#title_slide').val());
	// 	 formData.append('image', $('input[type=file]'));



	// 	 $.ajax({
 //        url : '/admin/save_main_slide',
 //        type : 'POST',
 //        dataType: "html",

 //        processData: false,
 //        data :formData,
 //        success : function (msg){
 //            alert(msg);
 //        }
 //    });

	// });



	// $(".s_img").change(function(ev){
	// 	//onchange="$('#my_form').submit();this.value='';"
	// 	var img = $(this).val();

	// 	$.ajax({
	//  	url: '/admin/save_image',
	//  	type: "POST",
	//  	data :({'_token': $('input[name=_token]').val()}),
	//  	dataType: "html",

	//  	success: function(data, d){
	//  			it.prev().remove();
	//  			it.remove();

	//  		}
	//  });

	// });


	// $('#add_ua_article').submit(function(ev){
	// 	if($('input[name=ua_name]').val() == ''){
	// 		alert('Заполните имя статьи!');
	// 		ev.preventDefault();
	// 	}

	// });



});
