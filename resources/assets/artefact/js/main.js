  $(document).ready(function() {

      var total_price = "";

      $('#fullpage').fullpage({
        sectionsColor: ['#1bbc9b', '#f9f9fa', '#fcfcfc', '#fafafa', '#fff'],
        anchors: ['firstPage', 'secondPage', '3rdPage', '4thpage', '5thpage'],
        menu: '#menu',
                // verticalCentered:false,
        scrollingSpeed: 1000
      });

            $("#owl-demo").owlCarousel({
 stopOnHover:true,
      autoPlay: 6000,
      items : 5,
      itemsDesktop : [1200,3],
        itemsTablet: [800, 2] ,
        itemsTabletSmall :[736, 5],
        itemsMobile : [414, 1],

  });

        $("#owl-demo2").owlCarousel({
            stopOnHover:true,
    autoPlay: 6000,
      items : 5,
      itemsDesktop : [1200,3],
        itemsTablet: [800, 2] ,
        itemsTabletSmall :[736, 5],
        itemsMobile : [414, 1],

  });
        $("#owl-demo3").owlCarousel({
 stopOnHover:true,
     autoPlay: 6000,
      items : 5,
      itemsDesktop : [1200,3],
        itemsTablet: [800, 2] ,
        itemsTabletSmall :[736, 5],
        itemsMobile : [414, 1],

  });
        $("#owl-demo4").owlCarousel({
 stopOnHover:true,
    autoPlay: 6000,
      items : 5,
      itemsDesktop : [1200,3],
        itemsTablet: [800, 2] ,
        itemsTabletSmall :[736, 5],
        itemsMobile : [414, 1],

  });
             $("#owl-demo5").owlCarousel({
 stopOnHover:true,
      autoPlay: 6000,
      items: 5,
      itemsDesktop : [1200,3],
        itemsTablet: [800, 2] ,
        itemsTabletSmall :[736, 4.5],
        itemsMobile : [414, 1],

  });

            $("#owl-shop-item").owlCarousel({
                navigation : true,
                slideSpeed : 800,
                paginationSpeed : 500,
                singleItem:true,
                autoPlay: false
            });

               $(".uk-lang").click(function(){
               $(".uk-lang").addClass("active-lang");
               $(".ru-lang").removeClass("active-lang");
               $(".en-lang").removeClass("active-tab");
            });
            $(".ru-lang").click(function(){
               $(".ru-lang").addClass("active-lang");
               $(".uk-lang").removeClass("active-lang");
               $(".en-lang").removeClass("active-lang");
            });
            $(".en-lang").click(function(){
               $(".en-lang").addClass("active-lang");
               $(".ru-lang").removeClass("active-lang");
               $(".uk-lang").removeClass("active-lang");
            });

               $(".cd-nav-trigger").click(function(){
               $(".cd-slideshow-nav").toggleClass("nav-open");

            });

                $(".dec-li").click(function(){
                $("#ul-tab").children().removeClass("active-li");
               $(this).addClass("active-li");

            });
            $(".pag-li").click(function(){
                $("#pag-ul").children().removeClass("active-li");
                $(this).addClass("active-li");

            });

            $('#tabs').tabs({
                hide: {
                    effect: "slide",
                    duration: 350,

                },
                show: {
                    effect: "slide",
                    duration:350,
                    direction:'right'
                }
            });

            setTimeout(function () {
                $('#section0').css("margin-top" , "-1px");
            }, 1);

                $( "#phone-form" ).click(function() {

                if ( $("#phone-form").val().length<1)
                $("#phone-form").val('+');
            });

            $(".search-nav").click(function(){
                $(".search-inner").toggleClass('search-op');
            });

            // $(document).mouseup(function (e){ // событие клика по веб-документу
            //     var div = $(".search-inner"); // тут указываем ID элемента
            //     if (!div.is(e.target) // если клик был не по нашему блоку
            //         && div.has(e.target).length === 0) { // и не по его дочерним элементам
            //         div.hide(); // скрываем его
            //     }
            // });

            $(".dropdown-date").click(function(){
                    $(".dropdown-content").toggle();
            });

            $(document).mouseup(function (e){ // событие клика по веб-документу
                var div = $(".dropdown-content"); // тут указываем ID элемента
                if (!div.is(e.target) // если клик был не по нашему блоку
                    && div.has(e.target).length === 0) { // и не по его дочерним элементам
                    div.hide(); // скрываем его
                }
            });


            $('.act-drop').click(function(e){
                var text = $(this).text();
                $('#drop-text').text(text);
            });

            $(".dropdown-xs").click(function(){
                $(".dropdown-xs-content").toggle();
            });

            $(document).mouseup(function (e){ // событие клика по веб-документу
                var div = $(".dropdown-xs-content"); // тут указываем ID элемента
                if (!div.is(e.target) // если клик был не по нашему блоку
                    && div.has(e.target).length === 0) { // и не по его дочерним элементам
                    div.hide(); // скрываем его
                }
            });


            $('.act-drop2').click(function(e){
                var text = $(this).text();
                $('#drop-text2').text(text);
            });



            $('.item-btn-buy').click(function () {
                $('.item-btn-buy').hide();
                $('.check').css({
                    'transition': '0.7s ease-in-out',
                    'stroke-dashoffset': 0,
                });
                $('.added-item').css({
                    'color':'#000',
                    'transition': '0.5s ease-in-out'
                });
            });


            $('.minus').click(function () {


                var quant = parseInt($(this).prev().prev().val());
                var price = parseInt($(this).closest('.basket-item').find('.price').text());
                var curent_total = parseInt($('.sum-bask').text());
                 if(quant != 1){
                    curent_total = curent_total - price;
                    curent_total = (curent_total < 0) ? 0 : curent_total;
                    $('.sum-bask').text(curent_total+"$");
                    total_price = curent_total+"$";
                }

                var $input = $(this).parent().find('input');
                var count = parseInt($input.val()) - 1;
                count = count < 1 ? 1 : count;
                $input.val(count);
                $input.change();

                 quant = parseInt($(this).prev().prev().val());
                 price = parseInt($(this).closest('.basket-item').find('.price').text());
                 curent_total = parseInt($('.sum-bask').text());

                $(this).closest('.basket-item').find('.total-price').text(price*quant + "$");
                


                return false;
            });
            $('.plus').click(function () {


                var $input = $(this).parent().find('input');
                $input.val(parseInt($input.val()) + 1);
                $input.change();

                var quant = parseInt($(this).prev().val());
                var price = parseInt($(this).closest('.basket-item').find('.price').text());
                var curent_total = parseInt($('.sum-bask').text());
                curent_total = curent_total + price;
                $('.sum-bask').text(curent_total+"$");
                total_price = curent_total+"$";



                 $(this).closest('.basket-item').find('.total-price').text(price*quant + "$");

                return false;
            });

            if ($(window).width() < 501) {
                $(".basket-item").each(function (i) {
                    var ch = $(this).children('.mobile-block');
                    $(ch).wrapAll('<div class="hyajax"></div>');
                    //$(ch).wrapAll('<div></div>');
                });
                // $('.basket-item h3, .basket-item small, .number, .basket-item p, .del-item').wrapAll('<div class="xs-align"></div>');
            }
            else {
                $('.basket-item').remove('.xs-align')
            }
            $(window).resize(function() {
                if ($(window).width() > 500) {
                    $(".basket-item").each(function (i) {
                        var ch = $(this).children('.hyajax');
                        var ch1 = $(ch).children('.mobile-block');
                        $(ch1).unwrap();
                        //$(ch).wrapAll('<div></div>');
                    });
                    // $('.basket-item h3, .basket-item small, .number, .basket-item p, .del-item').wrapAll('<div class="xs-align"></div>');
                }

                if ($(window).width() < 501) {
                    $(".basket-item").each(function (i) {
                        var ch = $(this).children('.mobile-block');
                        $(ch).wrapAll('<div class="hyajax"></div>');
                        //$(ch).wrapAll('<div></div>');
                    });
                    // $('.basket-item h3, .basket-item small, .number, .basket-item p, .del-item').wrapAll('<div class="xs-align"></div>');
                }
            });


            $('.add-to-basket').click(function(){
                var product = $(this).attr('data-product');


                $.ajax({
                     url: '/add-to-basket',
                     type: "POST",
                     data :({'_token': $('input[name=_token]').val(), 'product': product }),
                     dataType: "html",
                     success: function(data, d){

                       $('.counter-item').text(data);

                    }
                });


            });


             $('.del-item').click(function(){

               $('#product_count').text(parseInt($('#product_count').text()) - 1);

                var quant = parseInt($(this).prev().prev().find('.quant').val());
                var price = parseInt($(this).closest('.basket-item').find('.price').text());
                var curent_total = parseInt($('.sum-bask').text());
                var diference = quant * price;
                var new_total = curent_total - diference;
                $('.sum-bask').text(new_total+"$");
                total_price = new_total+"$";




                var product = $(this).attr('data-product'), it = $(this);



                $.ajax({
                     url: '/delete-from-basket',
                     type: "POST",
                     data :({'_token': $('input[name=_token]').val(), 'product': product }),
                     dataType: "html",
                     success: function(data, d){

                      it.parent('.basket-item').fadeOut(1500);

                    }
                });


            });
                 var total = 0;

             $('.basket-item').each(function(){
                    var totalprice = parseInt($(this).find('.price').text()) * parseInt($(this).find('.quant').val());
                    $(this).find('.total-price').text(totalprice + '$');
                    total+=totalprice;
             });
                $('.sum-bask').text(total+"$");
                total_price = total+"$";


         $('#add_basket_to_session').click(function(ev){
           ev.preventDefault();
           var i = 0;
           var basket_array = [];

            $('.basket-item').each(function(){
                var product_category = $(this).attr('data-category');
                var product_id = $(this).attr('data-product');
                var product_quant = parseInt($(this).find('.quant').val());
                var product_price =  parseInt($(this).find('.price').text());
                var totalProductPrice = product_quant * product_price;

                var basket_object = {
                    'product_id': product_id,
                    'product_category': product_category,
                    'product_quant': product_quant,
                    'product_price': product_price,
                    'totalProductPrice': totalProductPrice
                }

                basket_array[i] = basket_object;
                i++;

            });

            var totalPrice = parseInt(total_price);

             $.ajax({
                     url: '/add_basket_to_session',
                     type: "POST",
                     data :({'_token': $('input[name=_token]').val(), 'basket_array': JSON.stringify(basket_array), 'totalPrice': totalPrice}),
                     dataType: "html",
                     success: function(data, d){
                       location.href = 'client_info';
                    }
                });

         });

        $('#send_client_form').click(function(ev){
          ev.preventDefault();

          $("#signupForm").validate({
                     rules: {

                         country:"required",
                         city:"required",
                         region:"required",
                         index: {
                             required: true,
                             minlength: 4
                         },
                         adress:"required",
                         email: {
                             required: true,
                             email: true
                         },
                         phone:"required",
                         name: "required",
                         surname: "required",
                         droptext3:"required",
                         address_department: "required"

                     },
                     messages: {
                         country: "Це поле обов'язкове. Ввведіть країну",
                         city: "Це поле обов'язкове. Введіть місто",
                         region: "Це поле обов'язкове. Введіть область",
                         index: "Це поле обов'язкове. Введіть індекс",
                         adress: "Це поле обов'язкове. Введіть адресу",
                         email: "Це поле обов'язкове. Введіть адресу вірно",
                         phone: "Це поле обов'язкове. Введіть телефон в форматі +38 (032) 12 12 865",
                         name: "Це поле обов'язкове. Введіть ім'я",
                         surname: "Це поле обов'язкове. Введіть прізвище",
                         droptext3: "Це поле обов'язкове. Оберіть службу доставки",
                         address_department: "Це поле обов'язкове. Введіть адресу відділеня"
                     }
                 });

          $('#signupForm').submit();
        });

        $('.act-drop3').click(function(e){
                var text = $(this).text();
                $('#droptext3').val(text);
          });







    });