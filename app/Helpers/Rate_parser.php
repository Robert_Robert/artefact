<?php

namespace App\Helpers;

class Rate_parser {

	public $RUB = 0;
	public $USD = 0;

	public function get_all_rates(){
		$url = 'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json';
		$data = json_decode(file_get_contents($url));
		return $data;
	}

	public function get_rub(){
		$rates = $this->get_all_rates();
		foreach ($rates as $rate ) {
			if($rate->cc == 'RUB') $this->RUB = $rate->rate;
		}

		return $this->RUB;
	}

	public function get_usd(){
		$rates = $this->get_all_rates();
		foreach ($rates as $rate ) {
			if($rate->cc == 'USD') $this->USD = $rate->rate;
		}

		return $this->USD;
	}

}