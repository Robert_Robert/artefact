<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Helpers\Rate_parser;

class RateProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Helpers\Rate_parser', function(){
            return new Rate_parser();
        });
    }
}
