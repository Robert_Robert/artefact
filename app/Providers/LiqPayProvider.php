<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Helpers\LiqPay;

class LiqPayProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
         $this->app->bind('App\Helpers\LiqPay', function(){
            return new LiqPay();
        });
    }
}
