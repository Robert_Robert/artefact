<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Closure;

class MyForAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
   
      public function handle($request, Closure $next)
    {
        if (Auth::guest() || $request->user()->is_admin != 1) {
                return redirect('admin');
            } 

        return $next($request);
    }
}
