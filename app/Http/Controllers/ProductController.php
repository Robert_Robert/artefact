<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Main_category;
use App\Models\Sub_category;
use App\Models\Product;
use App\Models\Product_images;
use App\Models\Product_schema;
use App\Models\Product_images_icon;
use App\Helpers\Rate_parser;

use App\Http\Requests;
use Illuminate\Http\Response;
use Validator;
use App;
use Config;
use Session;
use DB;

class ProductController extends Controller
{
    public function show_product(Product $product){

    	$product_images = $product->product_images()->select('image')->get();
    	$product_schema = $product->product_schema()->select('image')->first();
      $product_model = $product->product_model()->select('model')->first();

      $sub_category = $product->sub_category()->select('id')->first()->id;

    	$main_category = $product->main_category()->select('ua_name', 'ru_name', 'en_name')->first();


    	$prev_product_id = DB::table('product')->where('id', '=', $product->id - 1)->where('sub_category', '=', $sub_category)->get()->isEmpty() ? DB::table('product')->where('sub_category', '=', $sub_category)->max('id') : DB::table('product')->where('id', '=', $product->id - 1)->where('sub_category', '=', $sub_category)->first()->id;

     	$next_product_id = DB::table('product')->where('id', '=', $product->id + 1)->where('sub_category', '=', $sub_category)->get()->isEmpty() ? DB::table('product')->where('sub_category', '=', $sub_category)->min('id') : DB::table('product')->where('id', '=', $product->id + 1)->where('sub_category', '=', $sub_category)->first()->id;



    	return view('artefact.single_product', ['product'=>$product, 'images'=>$product_images, 'schema'=>$product_schema, 'model'=>$product_model, 'category'=>$main_category, 'next_product'=>$next_product_id, 'prev_product'=>$prev_product_id]);
    }

    public function search(Product $product, Request $request){

            if(isset($request->filter)){

                        $products = $product->search($request->search_line, $request->filter);

                        return view('artefact.search', ['products'=>$products]);

                } else {
                        $products = $product->search($request->search_line);


                        return view('artefact.search', ['products'=>$products]);
                }
            }


    public function add_to_basket(Request $request){
        if(isset($request->product)){

             if(Session::get('product.basket')) {

               if(!in_array($request->product, Session::get('product.basket'))) $request->session()->push('product.basket', $request->product);


            }   else {
                 $request->session()->push('product.basket', $request->product);
            }

         }
      ;

       # Session::forget('product.basket');
         return count(Session::get('product.basket'));



    }

    public static function getCountBasket(){
        echo count(Session::get('product.basket'));
    }



}
