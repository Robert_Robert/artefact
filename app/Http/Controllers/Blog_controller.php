<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\UA_article;
use App\Models\RU_article;
use App\Models\EN_article;

use App\Http\Requests;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;



class Blog_controller extends Controller
{



    /*admin part*/
    public function ua_article(){
    	return view('admin.ua_article');
    }

      public function ua_articles(UA_article $article){
     	$articles = $article->paginate(9);
    	return view('admin.ua_articles', ['articles'=>$articles]);
    }

     public function delete_ua_article(UA_article $id){
     	$id->delete();
    	return redirect()->back()->with('message', 'Стаття удалена');
    }

    public function edit_ua_article(UA_article $id){
     	
    	return view('admin.alone_ua_article', ['article'=>$id]);
    }

    public function edit_ua(UA_article $id, Request $request){

    	
         $validator = Validator::make(  
            array('Название_статьи' => $request->ua_name,
                  'Текст_статьи' => $request->ua_text
                ),
            array('Название_статьи' => 'required',
                  'Текст_статьи' => 'required'
                ));

        if ($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }

    	$id->ua_name = $request->ua_name;
    	$id->ua_text = $request->ua_text;
    	$id->save();

    	$articl_obj = new UA_article();
    	$articles = $articl_obj->all();
     	$request->session()->flash('message', 'Стаття '.$request->ua_name.' обновлена!');
     	return redirect('admin/blog/ua-articles');
    
    }

     public function add_ua_article(UA_article $article, Request $request){
      
    // Validator::make([
    //   	 	 'Нзвание' => $request->ua_name,
    //   	 	 'Текст' => $request->ua_text
    //   	 	], 
    //   	 	[
    //     	  'Название' => 'required',
    //    		  'Текст' => 'required'
    //  		])->validate();
      	 $this->validate($request, 
     	 	[
       		 'ua_name' => 'required',
       		 'ua_text' => 'required',
    ]);
         
          if($request->cover != null){
            $cover = $request->cover;
            $filename = uniqid('cover_').'.'.$cover->getClientOriginalExtension();
            $path = $cover->move('public/articles_cover', $filename);
            $path = str_replace('articles_cover\\', 'articles_cover/', $path);
         } else {
            $path = 'public/articles_cover/default.jpg';
         }
        
        $article->image = $path;
    	$article->ua_name = $request->ua_name;
    	$article->ua_text = $request->ua_text;

    	$article->save();
    	return redirect()->back()->with('message', 'Стаття добавлена');
    }

    public function ru_article(){
    	return view('admin.ru_article');
    }

     public function add_ru_article(RU_article $article, Request $request){

     	 $this->validate($request, 
     	 	[
       		 'ru_name' => 'required',
       		 'ru_text' => 'required',
    ]);

         if($request->cover != null){
            $cover = $request->cover;
            $filename = uniqid('cover_').'.'.$cover->getClientOriginalExtension();
            $path = $cover->move('public/articles_cover', $filename);
            $path = str_replace('articles_cover\\', 'articles_cover/', $path);
         } else {
            $path = 'public/articles_cover/default.jpg';
         }
        
        $article->image = $path;

    	$article->ru_name = $request->ru_name;
    	$article->ru_text = $request->ru_text;

    	$article->save();
    	return redirect()->back()->with('message', 'Статья добавлена');
    }

     public function ru_articles(RU_article $article){
     	$articles = $article->paginate(9);
    	return view('admin.ru_articles', ['articles'=>$articles]);
    }

    public function edit_ru_article(RU_article $id){
     	
    	return view('admin.alone_ru_article', ['article'=>$id]);
    }

    public function delete_ru_article(RU_article $id){
     	$id->delete();
    	return redirect()->back()->with('message', 'Стаття удалена');
    }

     public function edit_ru(RU_article $id, Request $request){

     	 $validator = Validator::make(  
            array('Название_статьи' => $request->ru_name,
                  'Текст_статьи' => $request->ru_text
                ),
            array('Название_статьи' => 'required',
                  'Текст_статьи' => 'required'
                ));

        if ($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }
    	$id->ru_name = $request->ru_name;
    	$id->ru_text = $request->ru_text;
    	$id->save();

    	$articl_obj = new RU_article();
    	$articles = $articl_obj->all();
     	
    	$request->session()->flash('message', 'Стаття '.$request->ua_name.' обновлена!');
     	return redirect('admin/blog/ru-articles');
    }

    public function en_article(){
    	return view('admin.en_article');
    }

   

    public function add_en_article(EN_article $article, Request $request){
    	 $this->validate($request, 
     	 	[
       		 'en_name' => 'required',
       		 'en_text' => 'required',
    ]);

         if($request->cover != null){
            $cover = $request->cover;
            $filename = uniqid('cover_').'.'.$cover->getClientOriginalExtension();
            $path = $cover->move('public/articles_cover', $filename);
         } else {
            $path = 'public/articles_cover/default.jpg';
            $path = str_replace('articles_cover\\', 'articles_cover/', $path);
         }
        
        $article->image = $path;

    	$article->en_name = $request->en_name;
    	$article->en_text = $request->en_text;

    	$article->save();
    	return redirect()->back()->with('message', 'Artcicle added');
    }

     public function en_articles(EN_article $article){
     	$articles = $article->paginate(9);
    	return view('admin.en_articles', ['articles'=>$articles]);
    }

    public function delete_en_article(EN_article $id){
     	$id->delete();
    	return redirect()->back()->with('message', 'Стаття удалена');
    }

    public function edit_en_article(EN_article $id){
     	
    	return view('admin.alone_en_article', ['article'=>$id]);
    }

     public function edit_en(EN_article $id, Request $request){

     	 $validator = Validator::make(  
            array('Название_статьи' => $request->en_name,
                  'Текст_статьи' => $request->en_text
                ),
            array('Название_статьи' => 'required',
                  'Текст_статьи' => 'required'
                ));

        if ($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }

    	$id->en_name = $request->en_name;
    	$id->en_text = $request->en_text;
    	$id->save();

    	$articl_obj = new RU_article();
    	$articles = $articl_obj->all();
     	
    	$request->session()->flash('message', 'Стаття '.$request->ua_name.' обновлена!');
     	return redirect('admin/blog/en-articles');
    }

    /*end of admin part*/



    public function blog(EN_article $en, RU_article $ru, UA_article $ua){

        $locale = config('app.locale');

        switch ($locale) {
            case 'ru':
                $articles = $ru->paginate(8);
                return view('artefact.main_blog', ['articles'=>$articles]);
                break;
            case 'ua':
                $articles = $ua->paginate(8);
                return view('artefact.main_blog', ['articles'=>$articles]);
                break;
            case 'en':
                $articles = $en->paginate(8);
                return view('artefact.main_blog', ['articles'=>$articles]);
                break;    
            default:
                # code...
                break;
        }

        //return view('artefact.main_blog');
    }

    public function single_article(Request $request, EN_article $en, RU_article $ru, UA_article $ua){
        $id = $request->id;

        $locale = config('app.locale');

        switch ($locale) {
            case 'ru':
                $article = $ru->where('id', $id)->first();
                return view('artefact.single_article', ['article'=>$article]);
                break;
            case 'ua':
                $article = $ua->where('id', $id)->first();
                return view('artefact.single_article', ['article'=>$article]);
                break;
            case 'en':
                $article = $en->where('id', $id)->first();
                return view('artefact.single_article', ['article'=>$article]);
                break;    
            default:
                # code...
                break;
        }

    }


}
