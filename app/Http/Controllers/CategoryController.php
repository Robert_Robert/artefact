<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Main_category;
use App\Models\Sub_category;
use App\Models\Product;
use App\Models\Product_images;
use App\Models\Product_schema;
use App\Models\Product_images_icon;
use App\Helpers\Rate_parser;

use App\Http\Requests;
use Illuminate\Http\Response;
use Validator;
use App;
use Config;
use Session;
use DB;


class CategoryController extends Controller
{
    public function show_category_all(Main_category $main_cat, Request $request){

    	$main_prev_category =  DB::table('main_category');
    	$main_next_category =  DB::table('main_category');
    	$prev_category = $main_prev_category->where('id', '=', $main_cat->id - 1)->get()->isEmpty() ? DB::table('main_category')->where('id', '=', DB::table('main_category')->max('id'))->first() : $main_prev_category->where('id', '=', $main_cat->id - 1)->first();

     	$next_category = $main_next_category->where('id', '=', $main_cat->id + 1)->get()->isEmpty() ? DB::table('main_category')->where('id', '=', DB::table('main_category')->min('id'))->first() : $main_next_category->where('id', '=', $main_cat->id + 1)->first();

    	switch ($request->filter) {
    
    		case 'min-price':
    			 $sub_categoryes = $main_cat->sub_category()->get();
       			 $products = $main_cat->product() ->orderBy('price', 'asc')->paginate(8);
       			 return view('artefact.shop-decor', ['sub_cats'=>$sub_categoryes, 'main_cat'=>$main_cat, 'products'=>$products, 'prev_category'=>$prev_category, 'next_category'=>$next_category]);
    			break;

    		case 'max-price':
    			 $sub_categoryes = $main_cat->sub_category()->get();
       			 $products = $main_cat->product() ->orderBy('price', 'desc')->paginate(8);
       			 return view('artefact.shop-decor', ['sub_cats'=>$sub_categoryes, 'main_cat'=>$main_cat, 'products'=>$products, 'prev_category'=>$prev_category, 'next_category'=>$next_category]);
    			break;

    		case 'date':
    		      $sub_categoryes = $main_cat->sub_category()->get();
       			 $products = $main_cat->product() ->orderBy('date', 'desc')->paginate(8);
       			 return view('artefact.shop-decor', ['sub_cats'=>$sub_categoryes, 'main_cat'=>$main_cat, 'products'=>$products, 'prev_category'=>$prev_category, 'next_category'=>$next_category]);
    			break;		
    		
    		default:

    			 $sub_categoryes = $main_cat->sub_category()->get();
       			 $products = $main_cat->product()->paginate(8);

       			 return view('artefact.shop-decor', ['sub_cats'=>$sub_categoryes, 'main_cat'=>$main_cat, 'products'=>$products, 'prev_category'=>$prev_category, 'next_category'=>$next_category]);

    			break;
    	}

     

     
    
       
	}

	public function show_sub_category(Main_category $main_cat, Sub_category $sub_cat, Request $request){

		$main_prev_category =  DB::table('main_category');
    	$main_next_category =  DB::table('main_category');
    	$prev_category = $main_prev_category->where('id', '=', $main_cat->id - 1)->get()->isEmpty() ? DB::table('main_category')->where('id', '=', DB::table('main_category')->max('id'))->first() : $main_prev_category->where('id', '=', $main_cat->id - 1)->first();

     	$next_category = $main_next_category->where('id', '=', $main_cat->id + 1)->get()->isEmpty() ? DB::table('main_category')->where('id', '=', DB::table('main_category')->min('id'))->first() : $main_next_category->where('id', '=', $main_cat->id + 1)->first();

    	switch ($request->filter) {
    		case 'min-price':
    			 $sub_categoryes = $main_cat->sub_category()->get();
       			 $products = $sub_cat->product() ->orderBy('price', 'asc')->paginate(8);
       			 return view('artefact.shop-decor', ['sub_cats'=>$sub_categoryes, 'main_cat'=>$main_cat, 'products'=>$products, 'sub_cat'=>$sub_cat, 'prev_category'=>$prev_category, 'next_category'=>$next_category]);
    			break;

    		case 'max-price':
    			 $sub_categoryes = $main_cat->sub_category()->get();
       			 $products = $sub_cat->product() ->orderBy('price', 'desc')->paginate(8);
       			 return view('artefact.shop-decor', ['sub_cats'=>$sub_categoryes, 'main_cat'=>$main_cat, 'products'=>$products, 'sub_cat'=>$sub_cat, 'prev_category'=>$prev_category, 'next_category'=>$next_category]);
    			break;

    		case 'date':
    		      $sub_categoryes = $main_cat->sub_category()->get();
       			 $products = $sub_cat->product() ->orderBy('date', 'desc')->paginate(8);
       			 return view('artefact.shop-decor', ['sub_cats'=>$sub_categoryes, 'main_cat'=>$main_cat, 'products'=>$products, 'sub_cat'=>$sub_cat, 'prev_category'=>$prev_category, 'next_category'=>$next_category]);
    			break;		
    		
    		default:

    			 $sub_categoryes = $main_cat->sub_category()->get();
       			 $products = $sub_cat->product()->paginate(8);
       			 return view('artefact.shop-decor', ['sub_cats'=>$sub_categoryes, 'main_cat'=>$main_cat, 'products'=>$products, 'sub_cat'=>$sub_cat, 'prev_category'=>$prev_category, 'next_category'=>$next_category]);

    			break;
    	}

       
	}


  public function shop(Main_category $main_categoryes, Sub_category $sub_categoryes, Product $product){

          $all_main_categoryes = $main_categoryes->all();
          $products = $product->paginate(8); 

        return view('artefact.main-shop', ['main_categoryes'=>$all_main_categoryes, 'products'=>$products]);
  }


	public static function sub_cat_list(int $id_main_category){

		$sub_categoryes = DB::table('sub_category')->where('main_category', '=', $id_main_category)->get();
		return $sub_categoryes;
	}

}

  