<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Main_category;
use App\Models\Sub_category;
use App\Models\Product;
use App\Models\Product_images;
use App\Models\Product_schema;
use App\Models\Product_images_icon;
use App\Models\Main_page_slider;
use App\Models\Main_page_main_slider;
use App\Models\Dmodel;
use App\Helpers\Rate_parser;
use DB;
use App\Http\Requests;
use Illuminate\Http\Response;
use Validator;
use Carbon\Carbon;
use App\Models\Order;
use App\Models\Order_delivery_info;
use App\Models\DeliveryService;
use App\Models\Shopper;
use App\Models\Shopper_discount;
use Illuminate\Support\Facades\Mail;
use Intervention\Image\Facades\Image;


class AdminController extends Controller
{
	public function __construct(){
		//$this->middleware('MyForAuth');
	}
    //
	public function index(){

# Carbon documentation https://scotch.io/tutorials/easier-datetime-in-laravel-and-php-with-carbon

# $date_start_mounth = new DateTime(date('Y-n-j'));
#$date_prev_mounth = clone($date_start_mounth);
#$date_prev_mounth->modify('-1 month');

	$cur_date = Carbon::create(date('Y'), date('n'), date('j'), 0 );
	$date_start_mounth = Carbon::create(date('Y'), date('n'), 1, 0 );
	$order_furniture_cur_mounth = DB::table('order_products')->where('category', '=', 2)->whereBetween('created_at', [$date_start_mounth, $cur_date])->sum('product_quant');
	$order_decor_cur_mounth = DB::table('order_products')->where('category', '=', 1)->whereBetween('created_at', [$date_start_mounth, $cur_date])->sum('product_quant');
	$order_iluminate_cur_mounth = DB::table('order_products')->where('category', '=', 3)->whereBetween('created_at', [$date_start_mounth, $cur_date])->sum('product_quant');

	$date_prev_mounth = Carbon::create(date('Y'), date('n'), 1, 0 )->subMonth();
	$order_furniture_prev_mounth = DB::table('order_products')->where('category', '=', 2)->whereBetween('created_at', [$date_prev_mounth, $date_start_mounth])->sum('product_quant');
	$order_decor_prev_mounth =     DB::table('order_products')->where('category', '=', 1)->whereBetween('created_at', [$date_prev_mounth, $date_start_mounth])->sum('product_quant');
	$order_iluminate_prev_mounth = DB::table('order_products')->where('category', '=', 3)->whereBetween('created_at', [$date_prev_mounth, $date_start_mounth])->sum('product_quant');

	$date_two_prev_mounth = $date_six_prev_mounth = Carbon::create(date('Y'), date('n'), 1, 0 )->subMonth()->subMonth();
	$order_furniture_prev_two_mounth = DB::table('order_products')->where('category', '=', 2)->whereBetween('created_at', [$date_two_prev_mounth, $date_prev_mounth])->sum('product_quant');
	$order_decor_prev_two_mounth = DB::table('order_products')->where('category', '=', 1)->whereBetween('created_at', [$date_two_prev_mounth, $date_prev_mounth])->sum('product_quant');
	$order_iluminate_prev_two_mounth = DB::table('order_products')->where('category', '=', 3)->whereBetween('created_at', [$date_two_prev_mounth, $date_prev_mounth])->sum('product_quant');

	$date_three_prev_mounth = Carbon::create(date('Y'), date('n'), 1, 0 )->subMonth()->subMonth()->subMonth();
	$order_furniture_prev_three_mounth = DB::table('order_products')->where('category', '=', 2)->whereBetween('created_at', [$date_three_prev_mounth, $date_two_prev_mounth])->sum('product_quant');
	$order_decor_prev_three_mounth = DB::table('order_products')->where('category', '=', 1)->whereBetween('created_at', [$date_three_prev_mounth, $date_two_prev_mounth])->sum('product_quant');
	$order_iluminate_prev_three_mounth = DB::table('order_products')->where('category', '=', 3)->whereBetween('created_at', [$date_three_prev_mounth, $date_two_prev_mounth])->sum('product_quant');

	$date_four_prev_mounth = Carbon::create(date('Y'), date('n'), 1, 0 )->subMonth()->subMonth()->subMonth()->subMonth();
	$order_furniture_prev_four_mounth = DB::table('order_products')->where('category', '=', 2)->whereBetween('created_at', [$date_four_prev_mounth, $date_three_prev_mounth])->sum('product_quant');
	$order_decor_prev_four_mounth = DB::table('order_products')->where('category', '=', 1)->whereBetween('created_at', [$date_four_prev_mounth, $date_three_prev_mounth])->sum('product_quant');
	$order_iluminate_prev_four_mounth = DB::table('order_products')->where('category', '=', 3)->whereBetween('created_at', [$date_four_prev_mounth, $date_three_prev_mounth])->sum('product_quant');

	$date_five_prev_mounth = Carbon::create(date('Y'), date('n'), 1, 0 )->subMonth()->subMonth()->subMonth()->subMonth()->subMonth();
	$order_furniture_prev_five_mounth = DB::table('order_products')->where('category', '=', 2)->whereBetween('created_at', [$date_five_prev_mounth, $date_four_prev_mounth])->sum('product_quant');
	$order_decor_prev_five_mounth = DB::table('order_products')->where('category', '=', 1)->whereBetween('created_at', [$date_five_prev_mounth, $date_four_prev_mounth])->sum('product_quant');
	$order_iluminate_prev_five_mounth = DB::table('order_products')->where('category', '=', 3)->whereBetween('created_at', [$date_five_prev_mounth, $date_four_prev_mounth])->sum('product_quant');

	$date_six_prev_mounth = Carbon::create(date('Y'), date('n'), 1, 0 )->subMonth()->subMonth()->subMonth()->subMonth()->subMonth()->subMonth();
	$order_furniture_prev_six_mounth = DB::table('order_products')->where('category', '=', 2)->whereBetween('created_at', [$date_five_prev_mounth, $date_six_prev_mounth])->sum('product_quant');
	$order_decor_prev_five_mounth = DB::table('order_products')->where('category', '=', 1)->whereBetween('created_at', [$date_five_prev_mounth, $date_six_prev_mounth])->sum('product_quant');
	$order_iluminate_prev_five_mounth = DB::table('order_products')->where('category', '=', 3)->whereBetween('created_at', [$date_five_prev_mounth, $date_six_prev_mounth])->sum('product_quant');

	$cur_date = $cur_date->toDateString();
	$date_start_mounth = $date_start_mounth->toDateString();
	$date_prev_mounth = $date_prev_mounth->toDateString();
	$date_two_prev_mounth = $date_two_prev_mounth->toDateString();
	$date_three_prev_mounth = $date_three_prev_mounth->toDateString();
	$date_four_prev_mounth = $date_four_prev_mounth->toDateString();
	$date_five_prev_mounth = $date_five_prev_mounth->toDateString();
	$date_six_prev_mounth = $date_six_prev_mounth->toDateString();

		 if(Auth::check()){
		 	return view('admin.admin-main', ['cur_date'=>$cur_date,
			 'date_start_mounth'=>$date_start_mounth,
			 'date_prev_mounth'=>$date_prev_mounth,
			 'date_two_prev_mounth'=>$date_two_prev_mounth,
			 'date_three_prev_mounth'=>$date_three_prev_mounth,
			 'date_four_prev_mounth'=>$date_four_prev_mounth,
			 'date_five_prev_mounth'=>$date_five_prev_mounth,
			 'date_six_prev_mounth'=>$date_six_prev_mounth,
			 'order_furniture_cur_mounth'=>$order_furniture_cur_mounth,
			 'order_decor_cur_mounth'=>$order_decor_cur_mounth,
			 'order_iluminate_cur_mounth'=>$order_iluminate_cur_mounth,
			 'order_furniture_prev_mounth'=>$order_furniture_prev_mounth,
			 'order_decor_prev_mounth'=>$order_decor_prev_mounth,
			 'order_iluminate_prev_mounth'=>$order_iluminate_prev_mounth,
			 'order_furniture_prev_two_mounth'=>$order_furniture_prev_two_mounth,
			 'order_decor_prev_two_mounth'=>$order_decor_prev_two_mounth,
			 'order_iluminate_prev_two_mounth'=>$order_iluminate_prev_two_mounth,
			'order_furniture_prev_three_mounth'=>$order_furniture_prev_three_mounth,
			'order_decor_prev_three_mounth'=>$order_decor_prev_three_mounth,
			'order_iluminate_prev_three_mounth'=>$order_iluminate_prev_three_mounth,
			'order_furniture_prev_four_mounth'=>$order_furniture_prev_four_mounth,
			'order_decor_prev_four_mounth'=>$order_decor_prev_four_mounth,
			'order_iluminate_prev_four_mounth'=>$order_iluminate_prev_four_mounth,
			'order_furniture_prev_five_mounth'=>$order_furniture_prev_five_mounth,
			'order_decor_prev_five_mounth'=>$order_decor_prev_five_mounth,
			'order_iluminate_prev_five_mounth'=>$order_iluminate_prev_five_mounth,
			'order_furniture_prev_six_mounth'=>$order_furniture_prev_six_mounth,
			'order_decor_prev_six_mounth'=>$order_decor_prev_five_mounth,
			'order_iluminate_prev_six_mounth'=>$order_iluminate_prev_five_mounth,

		]);
		 } else {
		 	return view('auth.login');
		 }

	}

	public function date_statistic(Request $request){
		if($request->calculate){
				$furniture = DB::table('order_products')->where('category', '=', 2)->whereBetween('created_at', [$request->start_date, $request->end_date])->get()->count();
				$decor = DB::table('order_products')->where('category', '=', 1)->whereBetween('created_at', [$request->start_date, $request->end_date])->get()->count();
				$iluminate = DB::table('order_products')->where('category', '=', 3)->whereBetween('created_at', [$request->start_date, $request->end_date])->get()->count();
			  return view('admin.date_statistic', ['furniture'=>$furniture, 'decor'=>$decor, 'iluminate'=>$iluminate]);
		} else {
			  $cur_date = Carbon::create(date('Y'), date('n'), date('j'), 0 );
				$prev_month = Carbon::create(date('Y'), date('n')-1, date('j'), 0 );
				$furniture = DB::table('order_products')->where('category', '=', 2)->whereBetween('created_at', [$cur_date, $prev_month])->get()->count();
				$decor = DB::table('order_products')->where('category', '=', 1)->whereBetween('created_at', [$cur_date, $prev_month])->get()->count();
				$iluminate = DB::table('order_products')->where('category', '=', 3)->whereBetween('created_at', [$cur_date, $prev_month])->get()->count();
				return view('admin.date_statistic', ['furniture'=>$furniture, 'decor'=>$decor, 'iluminate'=>$iluminate]);
		}

	}

	public function client_statistic(Request $request, Shopper $shopper){

		$shoppers = $shopper->all();


		if($request->calculate){

				$furniture = DB::table('shopper_purchases')->where('status', '=', 'оплачено')->where('category', '=', 2)->where('shopper_id', '=', $request->client)->whereBetween('created_at', [$request->start_date, $request->end_date])->sum('product_quant');
				$decor = DB::table('shopper_purchases')->where('status', '=', 'оплачено')->where('category', '=', 1)->where('shopper_id', '=', $request->client)->whereBetween('created_at', [$request->start_date, $request->end_date])->sum('product_quant');
				$iluminate = DB::table('shopper_purchases')->where('status', '=', 'оплачено')->where('category', '=', 3)->where('shopper_id', '=', $request->client)->whereBetween('created_at', [$request->start_date, $request->end_date])->sum('product_quant');

				$furniture_cash = DB::table('shopper_purchases')->where('status', '=', 'оплачено')->where('category', '=', 2)->where('shopper_id', '=', $request->client)->whereBetween('created_at', [$request->start_date, $request->end_date])->sum('total_product_price');
				$decor_cash = DB::table('shopper_purchases')->where('status', '=', 'оплачено')->where('category', '=', 1)->where('shopper_id', '=', $request->client)->whereBetween('created_at', [$request->start_date, $request->end_date])->sum('total_product_price');
				$iluminate_cash = DB::table('shopper_purchases')->where('status', '=', 'оплачено')->where('category', '=', 3)->where('shopper_id', '=', $request->client)->whereBetween('created_at', [$request->start_date, $request->end_date])->sum('total_product_price');

				$total_cash = DB::table('shopper_purchases')->where('status', '=', 'оплачено')->where('shopper_id', '=', $request->client)->whereBetween('created_at', [$request->start_date, $request->end_date])->sum('total_product_price');

				  return view('admin.client_statistic', ['calculate'=>true, 'shoppers'=>$shoppers,
				  			  'furniture'=>$furniture, 'decor'=>$decor, 'iluminate'=>$iluminate,
				  			  'furniture_cash'=>$furniture_cash, 'decor_cash'=>$decor_cash,
				  			  'iluminate_cash'=>$iluminate_cash, 'total_cash'=>$total_cash, 'client'=>$request->client ]);

				// return view('admin.client_statistic', ['calculate'=>true, 'shoppers'=>$shoppers, 'furniture'=>3, 'decor'=>6, 'iluminate'=>9]);
		} else {
				return view('admin.client_statistic', ['shoppers'=>$shoppers]);
		}
	}


	public function orders_history(Order $order){
		$orders = $order->orderBy('created_at', 'desc')->paginate(10);
		return view('admin.orders_list', ['orders'=>$orders]);
	}

	public function order_detail(Order $order){
		$delivery = $order->order_delivery_info()->first();
		$products = $order->order_products()->get();

		return view('admin.order-details', ['order'=>$order, 'delivery'=>$delivery, 'products'=>$products]);
	}

	public function show_delivery_form(Order_delivery_info $delivery){
		return view('admin.edit_delivery_form', ['delivery'=>$delivery]);
	}

	public function delivery_services(DeliveryService $delivery){
		$services = $delivery->all();
		return view('admin.delivery_services', ['services'=>$services]);
	}

	public function add_delivery_sevice(request $request, DeliveryService $service){
		$validator = Validator::make(
		 array(
			   'На_русском'	=> $request->ru_name,
			   'На_украинском'	=> $request->ua_name,
			   'На_английском'	=> $request->en_name
			 ),
		 array(
			   'На_русском' => 'required',
			   'На_украинском' => 'required',
			   'На_английском' => 'required'
			 ));

			 if ($validator->fails()) {
 					return redirect()
 								->back()
 											->withErrors($validator)
 											->withInput();
 			}

 			$service->ru_name = $request->ru_name;
 			$service->ua_name = $request->ua_name;
 			$service->en_name = $request->en_name;
 			$service->save();

 		return redirect()->back()->with('message', 'Служба добавлена');


	}


	public function delete_service(DeliveryService $service){
		$service->delete();
		return redirect()->back()->with('message', 'Сервис удален');
	}

	public function save_delivery_changes(Order_delivery_info $delivery, Request $request){
		$delivery->country = $request->country;
		$delivery->city = $request->city;
		$delivery->region = $request->region;
		$delivery->adress = $request->adress;
		$delivery->post_index = $request->post_index;
		$delivery->delivery_service = $request->delivery_service;
		$delivery->delivery_department = $request->delivery_department;
		$delivery->name = $request->name;
		$delivery->surname = $request->surname;
		$delivery->email = $request->email;
		$delivery->phone = $request->phone;
		$delivery->save();

		 return redirect('/admin/order-details/'.$delivery->order_id)->with('message', 'Инофрмация обновлена');
	}

	public function discount_view(Shopper $shopper){
		$shoppers = $shopper->all();
		return view('admin.add_discount', ['shoppers'=>$shoppers]);
	}

	public function add_discount(Request $request, Shopper_discount $discount){
		$validator = Validator::make(
		 array('Клиент' => $request->client,
				   'Размер_скидки'	=> $request->size_discount,
				   'Начальная_дата'	=> $request->start_date,
					 'Конечная_дата'	=> $request->end_date
			 ),
		 array('Клиент' => 'required',
					 'Размер_скидки' => 'required',
					 'Начальная_дата' => 'required',
					 'Конечная_дата' => 'required'
			 ));

			 if ($validator->fails()) {
 					return redirect()
 								->back()
 											->withErrors($validator)
 											->withInput();
 			}

			$check = DB::table('shopper_discounts')->where('shopper_id', '=', $request->client)->get()->count();
			if($check != 0) return redirect()->back()->with('message', 'Скидка для этого клиента уже существует');

			$discount->shopper_id = $request->client;
			$discount->discount_percentage = $request->size_discount;
			$discount->date_start = $request->start_date;
			$discount->date_end = $request->end_date;
			$discount->save();



			$mail = DB::table('shoppers')->where('id', '=', $request->client)->first()->email;
			$content = "Поздравляем, вам начислена скидка в размере $request->size_discount % на сайте <a href='artfact-design.com.ua'>ARTEFACT<a>";


			 Mail::send('mails.mail', ['content' => $content], function($message) use($mail)
			{

					$message->from('grom379@gmail.com', 'Pasha')->subject('welcome');
					$message->to($mail);

			});

		return redirect()->back()->with('message', 'Скидка добавлена');
	}

	public function discount_list(Shopper_discount $discount){
		$discounts = $discount->paginate(9);
		return view('admin.discount_list', ['discounts'=>$discounts]);

	}

	public function edit_discount_form(Shopper_discount $discount, Shopper $shopper){
		$shoppers = $shopper->all();
    return view('admin.edit_discount', ['discount'=>$discount, 'shoppers'=>$shoppers]);
	}

	public function edit_discount(Shopper_discount $discount, Request $request){
		$validator = Validator::make(
		 array('Клиент' => $request->client,
				   'Размер_скидки'	=> $request->size_discount,
				   'Начальная_дата'	=> $request->start_date,
					 'Конечная_дата'	=> $request->end_date
			 ),
		 array('Клиент' => 'required',
					 'Размер_скидки' => 'required',
					 'Начальная_дата' => 'required',
					 'Конечная_дата' => 'required'
			 ));

			 if ($validator->fails()) {
 					return redirect()
 								->back()
 											->withErrors($validator)
 											->withInput();
 			}

			$discount->shopper_id = $request->client;
			$discount->discount_percentage = $request->size_discount;
			$discount->date_start = $request->start_date;
			$discount->date_end = $request->end_date;
			$discount->save();

			return redirect()->back()->with('message', 'Скидка обновлена');
	}

	public function delete_discount(Shopper_discount $discount){
		$discount->delete();
		return redirect()->back()->with('message', 'Скидка удалена');
	}

	public function save_image(Request $request){

		$filename = uniqid('log_').'.'.$request->image->getClientOriginalExtension();
		$path = url($path = $request->image->move("public/image", $filename));
		$path = str_replace('image', 'image/', $path);

		 return "<script>top.$('.mce-btn.mce-open').parent().find('.mce-textbox').val('".$path."');</script>";
	}

	public function ext(){

		Auth::logout();
		return redirect('/admin');
	}

	public function product(){
		$main_category = Main_category::all();
		$sub_category = Sub_category::all();

		return view('admin.add_product',  ['main_category' => $main_category, 'sub_category' => $sub_category]);
	}

	public function delete_product(Product $product){
		$product->delete();

		return redirect()->back();
	}

	public function delete_image(Product_images $id){
		$id->delete();
		return "true";
	}

	public function delete_schema(Product_schema $id){
		$id->delete();
		return "true";
	}

	public function delete_model(Dmodel $id){
		$id->delete();
		return "true";
	}

	public function save_change_product(Product $product, Request $request, Product_images $product_images, Product_schema $product_schema, Sub_category $sub_category, Main_category $main_category, Dmodel $model){

    	 $validator = Validator::make(
    	 	array('Назва_товару' => $request->ua_name,
    	 		  'Название_товара'	=> $request->ru_name,
    	 		  'Product_name'	=> $request->en_name,
    	 		  'Опис_товару'	=> $request->ua_description,
    	 		  'Описание_товара'	=> $request->en_description,
    	 		  'Product_description'	=> $request->en_description,
    	 		  'Цена'	=> $request->price,
    	 		  'Категория' => $request->main_category,
    	 		  'Под-категория' => $request->sub_category,
    	 		  'Количество' => $request->main_quantity
    	 		),
            array('Назва_товару' => 'required',
            	  'Название_товара' => 'required',
            	  'Product_name' => 'required',
            	  'Опис_товару' => 'required',
            	  'Описание_товара' => 'required',
            	  'Product_description' => 'required',
            	  'Цена' => 'required',
            	  'Категория' => 'required',
            	  'Под-категория' => 'required'
            	));

        if ($validator->fails()) {
            return redirect()
            			->back()
                        ->withErrors($validator)
                        ->withInput();
        }


		$product->save_change($request, $product_images, $product_schema, $model);

		return redirect('admin/products_list/'.$product->main_category()->first()->id)->with('message', 'Именения сохранены');
	}

	public function add_product(Request $request, Product $product, Product_images $product_images, Product_schema $product_schema, Sub_category $sub_category, Main_category $main_category, Dmodel $model){

    	 $validator = Validator::make(
    	 	array('Назва_товару' => $request->ua_name,
    	 		  'Название_товара'	=> $request->ru_name,
    	 		  'Product_name'	=> $request->en_name,
    	 		  'Опис_товару'	=> $request->ua_description,
    	 		  'Описание_товара'	=> $request->en_description,
    	 		  'Product_description'	=> $request->en_description,
    	 		  'Цена'	=> $request->price,
    	 		  'Категория' => $request->main_category,
    	 		  'Под-категория' => $request->sub_category
    	 		),
            array('Назва_товару' => 'required',
            	  'Название_товара' => 'required',
            	  'Product_name' => 'required',
            	  'Опис_товару' => 'required',
            	  'Описание_товара' => 'required',
            	  'Product_description' => 'required',
            	  'Цена' => 'required',
            	  'Категория' => 'required',
            	  'Под-категория' => 'required'
            	));

        if ($validator->fails()) {
            return redirect()
            			->back()
                        ->withErrors($validator)
                        ->withInput();
        }

		$product->add_product($request, $product_images, $product_schema, $sub_category, $main_category, $model);

		return redirect()->back()->with('message', 'Товар добавлен');
	}

	public function products_list(Main_category $category, Product $product){
		$products = $category->product()->paginate(9);

	    return view('admin.products_list', ['products' => $products ]);
	}

	public function edit_product(Product $product, Main_category $main_category){

		$categoryes = $main_category->all();
		//$sub_category  = $product->sub_category()->get()[0];
		$sub_category  = $product->main_category()->get()[0]->sub_category()->get();
		$model =  $product->product_model()->get();
		$images = $product->product_images()->get();
		$schema = $product->product_schema()->get();

		//dd($schema);

		return view('admin.edit_product', ['main_categoryes'=> $categoryes, 'product'=> $product, 'sub_category'=>$sub_category, 'images'=> $images, 'schemas'=> $schema, 'models'=>$model]);
	}

	public function category(){
		return view('admin.add_category');
	}

	public function add_category(Request $request, Main_category $main_category){
		//return view('admin.add_category');
		$main_category->add_category($request);

		return redirect()->back()->with('message', 'Категория добавлена');
	}

	public function category_list(Main_category $category){
		$categoryes = $category->all();

		return view('admin.category_list', ['categoryes'=>$categoryes]);
	}

	public function save_change_category(Main_category $category, Request $request){
		$category->ru_name = $request->ru_name;
		$category->ua_name = $request->ua_name;
		$category->en_name = $request->en_name;

		if($request->image != null){

			$filename = uniqid('category_').'.'.$request->image->getClientOriginalExtension();
	 	 	$path = $request->image->move('public/category_images', $filename);
	 	 	$path = str_replace('\\', '/', $path);

	 	 	$category->image = $path;
		}
		$category->save();

		$main_categories = new Main_category();

		return view('admin/category_list', ['categoryes'=>$main_categories->all()]);
	}

	public function show_category(Main_category $id){

		return view('admin.edit_category', ['category' => $id]);
	}

	public function category_delete(Main_category $id){
		$name = $id->ru_name;
		$id->delete();
		 return redirect()->back()->with('message', 'Категория '.$name.' удалена');
	}

	public function sub_category(){

		$categotyes = Main_category::all();

		return view('admin.add_sub_category', ['categoryes' => $categotyes]);
	}

	public function add_sub_category(Request $request, Sub_category $sub_category){
		//return view('admin.add_category');
		$sub_category->add_category($request);
		return redirect()->back()->with('message', 'Под-категория добавлена');
	}

	public function ajax_sub_category(Request $request){
		$need_category =  $request->main_category;
		$category = new Sub_category();
		$sub_categoryes = $category->where('main_category', $need_category)->get();
		return response()->json($sub_categoryes);
	}

	public function slider(Main_page_slider $slider){
		$sliders = $slider->all();
		return view('admin.main_page_slider', ['images'=>$sliders]);
	}

	public function add_sliders(Request $request, Main_page_slider $slider){

		if($request->image != null){
	 	 foreach ($request->image as $image) {

	 	 	$filename = uniqid('product_').'.'.$image->getClientOriginalExtension();
	 	 	$path = $image->move('public\main_page_slider', $filename);
	 	 	$img = Image::make($path);
	 	 	$img->resize(220, 220);
	 	 	$img->save($path);
	 	 	$slider->insert(['image'=>$path]);

	 	 }
	 	  return redirect('admin/slider')->with('Слайды добавлено');
	 	}else{

	 	 return redirect('admin/slider');
	 	}

	}

	public function delete_slide(Main_page_slider $id){
		$id->delete();
		return "true";
	}

	public function main_slider(Main_page_main_slider $slider){
		$sliders = $slider->all();
		return view('admin.main_page_main_slider', ['images'=>$sliders]);
	}

	public function save_slide(Request $request, Main_page_main_slider $slider){

	 	 	$filename = uniqid('slider_').'.'.$request->image->getClientOriginalExtension();
	 	 	$path = $request->image->move('public/main_page_slider', $filename);
	 	 	$path = str_replace('\\', '/', $path);


		 	 	$id = $slider->insertGetId([
		 		'image' => $path,
		 		'ru_description' => $request->title_slide_ru,
		 		'ua_description' => $request->title_slide_ua,
		 		'en_description' => $request->title_slide_en,
		 		]);

	 	 	return redirect('admin/main_slider')->with('Слайд добавлено');

	}

	public function delete_main_slide(Request $request){
		 DB::table('main_page_main_slider')->where('id', '=', $request->id)->delete();
		echo $request->id;
	}

	public function update_slide_main_slider(Main_page_main_slider $slide, Request $request){

		$slide->ru_description = $request->title_slide_ru;
		$slide->ua_description = $request->title_slide_ua;
		$slide->en_description = $request->title_slide_en;
		$slide->save();

		return redirect()->back()->with('Слайд обновлен');
	}


}
