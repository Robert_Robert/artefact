<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Main_category;
use App\Models\Sub_category;
use App\Models\Product;
use App\Models\Product_images;
use App\Models\Product_schema;
use App\Models\Product_images_icon;
use App\Helpers\Rate_parser;
use App\Models\Main_page_slider;
use App\Models\Main_page_main_slider;

use App\Http\Requests;
use Illuminate\Http\Response;
use Validator;
use App;
use Config;
use Session;
//use Mail;
use Illuminate\Support\Facades\Mail;

class MainPageController extends Controller
{
    public function index(Main_page_slider $slider, Main_page_main_slider $main_slider, Main_category $main_category){
        $sliders = $slider->all();
        $main_slides = $main_slider->all();
        $main_categoryes = $main_category->all();

    	return view('artefact.main_page', ['slides'=>$sliders, 'main_slides'=>$main_slides, 'main_categoryes'=>$main_categoryes]);
    }

    public function change_language ($locale){

    	if (!in_array($locale, Config::get('app.locales'))) {
    		$locale = App::getLocale();
    	}
    	App::setLocale($locale);
    	Session::put('locale', $locale); 
    	return redirect()->back();
    }

    public function contacts(){
    	return view('artefact.contacts');
    }

    public function contacts_send_mail(Request $request){

    	if(App::getLocale() == 'ua'){
    	 $validator = Validator::make(  
    	 	array('Ім\'я' => $request->name,
    	 		  'Пошта'	=> $request->email,
    	 		  'Телефон'	=> $request->phone,
    	 		  'Повідомлення'	=> $request->content
    	 		),
            array('Ім\'я' => 'required',
            	  'Пошта' => 'required',
            	  'Телефон' => 'required',
            	  'Повідомлення' => 'required'
            	));
    	} elseif(App::getLocale() == 'ru'){
    		 $validator = Validator::make(  
    	 	array('Имя' => $request->name,
    	 		  'Почта'	=> $request->email,
    	 		  'Телефон'	=> $request->phone,
    	 		  'Сообщение'	=> $request->content
    	 		),
            array('Имя' => 'required',
            	  'Почта' => 'required',
            	  'Телефон' => 'required',
            	  'Сообщение' => 'required'
            	));
    	} else {
    		 $validator = Validator::make(  
    	 	array('Name' => $request->name,
    	 		  'Mail'	=> $request->email,
    	 		  'Phone'	=> $request->phone,
    	 		  'Message'	=> $request->content
    	 		),
            array('Name' => 'required',
            	  'Mail' => 'required',
            	  'Phone' => 'required',
            	  'Message' => 'required'
            	));
    	}

        if ($validator->fails()) {
            return redirect()
            			->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $name = $request->name;
        $phone = $request->phone;
        $mail = $request->email;
        $content = $request->content;

         Mail::send('mails.mail', ['content' => $content], function ($message)
        {

            $message->from('grom379@gmail.com', 'Pasha')->subject('welcome');

            $message->to('pavlo1@list.ru');

        });

         return redirect()->back();


    	
    }



    


}
