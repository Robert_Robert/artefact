<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use Illuminate\Http\Response;
use Validator;
use Illuminate\Support\Collection;
use Session;
use App\Models\Product;
use App\Helpers\LiqPay;
use App\Models\Order;
use App\Models\Order_delivery_info;
use App\Models\Order_product;
use App\Models\Shopper_purchase;
use App\Models\Shopper_discount;
use App\Models\Shopper;
use Illuminate\Routing\Redirector;
use Carbon\Carbon;


class BasketController extends Controller
{
    public function show_basket(Product $product_obj){

    	$products = collect([ ]);

    	 if(Session::get('product.basket')) {
    			foreach (Session::get('product.basket') as $item) {
    				$products->push($product_obj->getProductById($item));
    			}
    	}

    	return view('artefact.basket', ['tittle'=>'messages.basket', 'products'=>$products]);
    }

    public function delete_from_basket(Request $request){
    	if(isset($request->product)){
    		foreach(Session::get('product.basket') as $key => $value) {
    			if ($request->product == $value) {



        $event_data_display = Session::get('product.basket');
				unset($event_data_display[$key]);
				Session::set('product.basket', $event_data_display);


    			}
			}
    	}

    	echo count(Session::get('product.basket'));

    }

    public function client_info(){


    	return view('artefact.client_info');
    }

    public function add_basket_to_session(Request $request){

        $basket_array =  json_decode($request->basket_array);

        Session::set('product.order', $basket_array);
        // $request->session()->put('totalprice', $request->totalPrice);

        return 'work';

    }

    public function get_product_price(Product $product){
      return $product->price;
    }

    public function clear_basket_in_session(Request $request){
    	Session::set('product.order', []);
    }

    public function add_client_info(Request $request, Order $order){


       $client_info = [
         'country' => $request->country,
         'city' => $request->city,
         'region' => $request->region,
         'index' => $request->index,
         'adress' => $request->adress,
         'name' => $request->name,
         'surname' => $request->surname,
         'email' => $request->email,
         'phone' => $request->phone,
         'service_delivery' => $request->service_delivery,
         'address_department' => $request->address_department,
         'message' => $request->message
       ];

         Session::set('product.client_info', $client_info);


         return redirect('pay_page');

       

    }

    public function create_pay_page(Request $request){
     
      $global_order_price = 0;
        foreach(Session::get('product.order') as $product){

          $price = Product::where('id', '=', $product->product_id)->first()->price * $product->product_quant;
          $global_order_price += $price;
          
         }
      $request->session()->put('totalprice',  $global_order_price);   
      

       

         // $liqpay = new LiqPay('i86173540708', 'xc14p1XszeNUjZMr7qZRXLfWYQigYNnfnicA5MEy');
         //  $html = $liqpay->cnb_form(array(
         //  'action'         => 'pay',
         //  'amount'         => '1',
         //  'currency'       => 'USD',
         //  'description'    => 'description text',
         //  'order_id'       => 'order_id_1',
         //  'version'        => '3'
         //  ));

         //  echo "<a href='https://www.liqpay.com/api/3/checkout$html'>liqpay</a>";

        $email = Session::get('product.client_info')['email'];

       #отримую розмір знижки якщо вона є, якщо нема - 0 (скорочений запис)
         $discount = !empty(!empty(Shopper::where('email', '=', $email)->first()) ? Shopper_discount::where('shopper_id', '=', Shopper::where('email', '=', $email)->first()->id)->first() : 0) ? Shopper_discount::where('shopper_id', '=', Shopper::where('email', '=', $email)->first()->id)->first()->discount_percentage : 0;

         /*Якщо комусь хочеться розібратись то вот цей код більш розписаний:

          $shopper = Shopper::where('email', '=', $request->email)->first();

         $dis = !empty($shopper) ? Shopper_discount::where('shopper_id', '=', $shopper->id)->first() : 0;

         $discount = !empty($dis) ? $dis->discount_percentage : 0;

         */
       return view('artefact.pay-page', ['totalprice'=>Session::get('totalprice'), 'discount'=>$discount, 'products'=>Session::get('product.basket')]);
    }

    public function pay_request(Redirector $redirect, Order $order, Order_delivery_info $delivery,Shopper $shopper, Shopper_purchase $purchase, Request $request){

         $email = Session::get('product.client_info')['email'];
         $discount = !empty(!empty(Shopper::where('email', '=', $email)->first()) ? Shopper_discount::where('shopper_id', '=', Shopper::where('email', '=', $email)->first()->id)->first() : 0) ? Shopper_discount::where('shopper_id', '=', Shopper::where('email', '=', $email)->first()->id)->first()->discount_percentage : 0;

         $totalprice = Session::get('totalprice');

         $mainTotalPrice = ($discount > 0) ? Session::get('totalprice') / 100 * $discount : Session::get('totalprice');
         $message = !empty(Session::get('product.client_info')['message']) ? Session::get('product.client_info')['message'] : '';
         

         #step 1 - замовлення в таблицю orders
         $order_id = $order->insertGetId([
            'user_mail' => $email,
            'order_price' => $mainTotalPrice,
            'status' => 'неоплачено',
            'created_at' => Carbon::create(date('Y'), date('n'), date('j'), 0 ),
            'message' => $message
         ]);



         #step 2 - записую в order_product
         foreach(Session::get('product.order') as $product){

            DB::table('order_products')->insert(
              [
              'order_id' => $order_id,
              'product_id' => $product->product_id,
              'product_quant' => $product->product_quant,
              'product_price' => $product->product_price,
              'total_product_price' => $product->totalProductPrice,
              'category' => $product->product_category,
              'created_at' => Carbon::create(date('Y'), date('n'), date('j'), 0 )
               ]
            );
         }

         #step 3 - записую в order_delivery_info
  
         $delivery->order_id = $order_id;
         $delivery->country  =  Session::get('product.client_info')['country'];
         $delivery->city  =  Session::get('product.client_info')['city'];
         $delivery->region =  Session::get('product.client_info')['region'];
         $delivery->post_index  =  Session::get('product.client_info')['index'];
         $delivery->adress  =  Session::get('product.client_info')['adress'];
         $delivery->name  =  Session::get('product.client_info')['name'];
         $delivery->surname  =  Session::get('product.client_info')['surname'];
         $delivery->email  =  Session::get('product.client_info')['email'];
         $delivery->phone  =  Session::get('product.client_info')['phone'];
         $delivery->delivery_service  =  Session::get('product.client_info')['service_delivery'];
         $delivery->delivery_department  =  Session::get('product.client_info')['address_department'];
         $delivery->save();

      //    #step 4 - записую в таблицю shoppers
         $check_mail = empty(Shopper::where('email', '=',  Session::get('product.client_info')['email'])->first()) ? true : false;

         if($check_mail){
        
             $shopper_id = $shopper->insertGetId([
            'email' => Session::get('product.client_info')['email'],
            'name' => Session::get('product.client_info')['name'],
            'surname' => Session::get('product.client_info')['surname'],
            'phone' => Session::get('product.client_info')['phone'],
         ]);
         }  else {
             $shopper_id = Shopper::where('email', '=',  Session::get('product.client_info')['email'])->first()->id;
         }

         #step 5 - записую в таблицю shopper_purchases

          foreach(Session::get('product.order') as $product){

            DB::table('shopper_purchases')->insert(
              [
              'order_id' => $order_id,
              'shopper_id' => $shopper_id,
              'product_id' => $product->product_id,
              'product_quant' => $product->product_quant,
              'product_price' => $product->product_price,
              'total_product_price' => $product->totalProductPrice,
              'category' => $product->product_category,
              'created_at' => Carbon::create(date('Y'), date('n'), date('j'), 0 ),
              'status' => 'неоплачено'
               ]
            );
         }

         #step 6 - записую id замовлення в сесію
         $request->session()->put('order_id',  $order_id);  

         #step 7 - liqpay
           $liqpay = new LiqPay('i86173540708', 'xc14p1XszeNUjZMr7qZRXLfWYQigYNnfnicA5MEy');
           $html = $liqpay->cnb_form(array(
          'action'         => 'pay',
          'amount'         =>  1,
          'currency'       => 'USD',
          'description'    => 'description text',
          'order_id'       =>  $order_id,
          'version'        => '3',
          'language'       => config('app.locale'),
          'sandbox'        => 1,

          ));

         

      return $redirect->to("https://www.liqpay.com/api/3/checkout$html");
    }
}

