<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EN_article extends Model
{
    
     protected $table = 'en_articles';
	 public $timestamps = false;
	 protected $fillable = ['en_name', 'en_text', 'date'];
 
	
}
