<?php

namespace App\Models;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Database\Eloquent\Model;
use  App\Models\Product_images;
use App\Models\Product_schema;
use App\Models\Main_category;
use App\Models\Sub_category;
use App\Models\Product_images_icon;
use Intervention\Image\Facades\Image;
use DB;
use App\Models\Dmodel;

class Shopper extends Model
{
  protected $table = 'shoppers';
  public $timestamps = false;
  protected $fillable = ['email', 'name', 'surname', 'phone', 'total_sum'];

  public function shopper_purchases(){
   return $this->hasMany('App\Models\Shopper_purchase', 'shopper_id');
  }

   public function discount(){
	 	 return $this->hasMany('App\Models\Shopper_discount', 'shopper_id');
	 }
}
