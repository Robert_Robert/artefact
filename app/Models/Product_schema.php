<?php

namespace App\Models;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Product;

use Illuminate\Database\Eloquent\Model;

class Product_schema extends Model
{
	 protected $table = 'product_schema';
	 public $timestamps = false;
	 protected $fillable = ['product_id', 'image'];

	  public function product()
	{
    		return $this->belongsTo('App\Models\Product');
	}   
}
