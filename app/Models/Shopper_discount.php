<?php

namespace App\Models;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Database\Eloquent\Model;
use  App\Models\Product_images;
use App\Models\Product_schema;
use App\Models\Main_category;
use App\Models\Sub_category;
use App\Models\Product_images_icon;
use Intervention\Image\Facades\Image;
use DB;
use App\Models\Dmodel;
use App\Models\Shopper;

class Shopper_discount extends Model
{
  protected $table = 'shopper_discounts';
  public $timestamps = false;
  protected $fillable = ['shopper_id', 'discount_percentage', 'date_start	', 'date_end'];

  public static function getMailName(int $id){
    $client = DB::table('shoppers')->where('id', '=', $id)->first();
    $result = $client->email . " " . $client->surname	. " " . $client->name;
    return $result;
  }

}
