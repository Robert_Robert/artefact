<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UA_article extends Model
{
    
     protected $table = 'ua_articles';
	 public $timestamps = false;
	 protected $fillable = ['ua_name', 'ua_text', 'date'];
 
	
}
