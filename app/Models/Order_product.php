<?php

namespace App\Models;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Database\Eloquent\Model;
use  App\Models\Product_images;
use App\Models\Product_schema;
use App\Models\Main_category;
use App\Models\Sub_category;
use App\Models\Product_images_icon;
use Intervention\Image\Facades\Image;
use DB;
use App\Models\Dmodel;

class Order_product extends Model
{
  protected $table = 'order_products';
  public $timestamps = false;
  protected $fillable = ['order_id', 'product_id', 'product_quant', 'product_price', 'total_product_price'];

  public function order(){
       return $this->belongsTo('App\Models\Order');
 }

}
