<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Product;

class Product_images_icon extends Model
{
    protected $table = 'products_images_icons';
	public $timestamps = false;
	protected $fillable = ['product_id', 'image'];

	 public function product()
	{
    		return $this->belongsTo('App\Models\Product');
	}
}
