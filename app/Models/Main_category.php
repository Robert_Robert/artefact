<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;

class Main_category extends Model
{
	 protected $table = 'main_category';
	 public $timestamps = false;
	 protected $fillable = ['ua_name', 'ru_name', 'en_name'];

	 public function add_category(Request $request){

	 	$this->ua_name = $request->ua_name;
	 	$this->ru_name = $request->ru_name;
	 	$this->en_name = $request->en_name;

	 	 if($request->image != null){
	 		$filename = uniqid('category_').'.'.$request->image->getClientOriginalExtension();
	 		$path = $request->image->move('public/category_images', $filename);
	 	
	 		$this->image = $path;
	 	 } else {
	 	 	$this->image = 'public\category_images\default_image.jpg';
	 	 }

	 	$this->save();
	 }

	 public static function get_all_category(){
	 	return self::all();
	 }

	  public static function getCategoryName(int $id){
	 	$category = DB::table('main_category')->where('id', '=', $id)->first()->ru_name;
	  	return $category;
	 }
    
    public function product(){
	 	 return $this->hasMany('App\Models\Product', 'category');
	 }

	  public function sub_category(){
	 	 return $this->hasMany('App\Models\Sub_category', 'main_category');
	 }



}
