<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Dmodel extends Model
{
     protected $table = 'd_model';
	 public $timestamps = false;
	 protected $fillable = ['model', 'product_id'];


	  public function sub_category()
	{
    		return $this->belongsTo('App\Models\Product', 'product_id');
	}
}
