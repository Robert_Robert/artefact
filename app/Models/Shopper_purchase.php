<?php

namespace App\Models;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Database\Eloquent\Model;
use  App\Models\Product_images;
use App\Models\Product_schema;
use App\Models\Main_category;
use App\Models\Sub_category;
use App\Models\Product_images_icon;
use Intervention\Image\Facades\Image;
use DB;
use App\Models\Dmodel;

class Shopper_purchase extends Model
{
  protected $table = 'shopper_purchases';
  public $timestamps = false;
  protected $fillable = ['shopper_id', 'product_id', 'product_quant', 'product_price', 'total_price', 'total_sum', 'status', 'created_at'];


  public function shopper(){
       return $this->belongsTo('App\Models\Shopper');
 }

}
