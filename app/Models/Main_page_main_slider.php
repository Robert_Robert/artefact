<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class Main_page_main_slider extends Model
{
    protected $table = 'main_page_main_slider';
	public $timestamps = false;
	protected $fillable = ['image', 'ru_description', 'ua_description', 'en_description'];


	public static function get_all_slides(){

	  	return DB::table('main_page_main_slider')->orderBy('id', 'desc')->get();
	}
}
