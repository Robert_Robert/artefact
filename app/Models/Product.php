<?php

namespace App\Models;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Database\Eloquent\Model;
use  App\Models\Product_images;
use App\Models\Product_schema;
use App\Models\Main_category;
use App\Models\Sub_category;
use App\Models\Product_images_icon;
use Intervention\Image\Facades\Image;
use DB;
use App\Models\Dmodel;

class Product extends Model
{
     protected $table = 'product';
	 public $timestamps = false;
	 protected $fillable = ['ua_name', 'ru_name', 'en_name', 'ua_description', 'ru_description', 'en_description', 'price', 'sub_category', 'category'];
	 public $small_img;

	

	 public static function getLatestMainPage($category){
	 	$products = DB::table('product')->where('category', '=', $category)->orderBy('id', 'desc')->take(10)->get();
	  	return $products;
	 }

	  public static function getLatest(){
	  	$products = DB::table('product')->orderBy('id', 'desc')->take(6)->get();
	  	return $products;
	  }

	  public static function getLatestForMain(){
	  	$products = DB::table('product')->orderBy('id', 'desc')->take(10)->get();
	  	return $products;
	  }

	  public static function get_first_image($id){
	  	$image = DB::table('product_images')->where('product_id', '=', $id)->take(1)->get();


	  	return $image;
	  //var_dump($image);

	  	// foreach ($image as $key => $value) {
	  	// 	foreach ($value as $key => $v) {
	  	// 		echo $v;
	  	// 	}
	  	// }
	  }


	  public function add_product(Request $request, Product_images $product_images, Product_schema $product_schema, Sub_category $sub_category, Main_category $main_category, Dmodel $model){


	  	
	 	
	 	$id = $this->insertGetId([
	 		'ua_name' => $request->ua_name,
	 		'ru_name' => $request->ru_name,
	 		'en_name' => $request->en_name,
	 		'ua_description' => $request->ua_description,
	 		'ru_description' => $request->ru_description,
	 		'en_description' => $request->en_description,
	 		'price' => $request->price,
	 		'category' => $request->main_category,
	 		'sub_category' => $request->sub_category,
	 		'main_quantity'  =>  $request->quantity,
	        'cur_quantity'  =>  $request->quantity
	 		 ]);
	 	
	 		if($request->image != null){
	 	 foreach ($request->image as $image) {
	 	  
	 	 	$filename = uniqid('product_').'.'.$image->getClientOriginalExtension();
	 	 	$path = $image->move('public\product_images', $filename);
	 	 	$img = Image::make($path);
	 	 	$img->resize(246, 246);
	 	 	$img->save($path);
	 	 	$product_images->insert(['product_id'=>$id, 'image'=>$path]);
	 	 	
	 	 }
	 	}


	 		// open an image file
// $img = Image::make('public/foo.jpg');

// // now you are able to resize the instance
// $img->resize(320, 240);

// // and insert a watermark for example
// $img->insert('public/watermark.png');

// // finally we save the image as a new file
// $img->save('public/bar.jpg');

	 	
	 	 if($request->schema != null){
	 	foreach ($request->schema as $schema) {
	 	
	 		$filename = uniqid('product_').'.'.$schema->getClientOriginalExtension();
	 		$path = $schema->move('public\product_schema', $filename);
	 		$product_schema->insert(['product_id'=>$id, 'image'=>$path]);
	 		
	 	 }
	 	}


	 	 if($request->model != null){
	 	
	 		$filename = uniqid('product_').'.'.$request->model->getClientOriginalExtension();
	 		$path = $request->model->move('public\3Dmodels', $filename);
	 		$model->insert(['product_id'=>$id, 'model'=>$path]);
	 		
	 	}
	 }

	 	public function save_change(Request $request, Product_images $product_images, Product_schema $product_schema, Dmodel $model){
		 	$this->ua_name = $request->ua_name;
	        $this->ru_name = $request->ru_name;
	        $this->en_name = $request->en_name;
	        $this->ua_description = $request->ua_description;
	        $this->ru_description = $request->ru_description;
	        $this->en_description = $request->en_description;
	        $this->price = $request->price;
	        $this->category = $request->main_category;
	        $this->sub_category = $request->sub_category;
	        $this->main_quantity  =  $request->quantity;
	        $this->cur_quantity  =  $request->quantity;

	        $this->save();

	      

	          if($request->image != null){
	         foreach ($request->image as $image) {
	 	
	 	 	$filename = uniqid('product_').'.'.$image->getClientOriginalExtension();
	 	 	$path = $image->move('public\product_images', $filename);
	 	 	$product_images->insert(['product_id'=>$this->id, 'image'=>$path]);
	 	 	
	 	 }
	 	}

	 		 if($request->schema != null){
	 		foreach ($request->schema as $schema) {
	 	
	 		$filename = uniqid('product_').'.'.$schema->getClientOriginalExtension();
	 		$path = $schema->move('public\product_schema', $filename);
	 		$product_schema->insert(['product_id'=>$this->id, 'image'=>$path]);
	 		
	 	 }
	 	}	


	 		 if($request->model != null){
	 	
	 		$filename = uniqid('product_').'.'.$request->model->getClientOriginalExtension();
	 		$path = $request->model->move('public\3Dmodels', $filename);
	 		$model->insert(['product_id'=>$this->id, 'model'=>$path]);
	 		
	 	}

	 	}

	 	public function search(String $search_line, String $filter = ''){

	 		
         


                 		switch ($filter) {
                 			case 'min-price':
                 				$results = $this->where('ua_name', 'LIKE', '%'.$search_line.'%')->orwhere('ru_name', 'LIKE', '%'.$search_line.'%')->orwhere('en_name', 'LIKE', '%'.$search_line.'%')->orderBy('price', 'asc')->paginate(8);
                    			return $results;     
                 			break;

                 			case 'max-price':
                 				$results = $this->where('ua_name', 'LIKE', '%'.$search_line.'%')->orwhere('ru_name', 'LIKE', '%'.$search_line.'%')->orwhere('en_name', 'LIKE', '%'.$search_line.'%')->orderBy('price', 'desc')->paginate(8);
                    			return $results;     
                 			break;

                 			case 'date':
                 				$results = $this->where('ua_name', 'LIKE', '%'.$search_line.'%')->orwhere('ru_name', 'LIKE', '%'.$search_line.'%')->orwhere('en_name', 'LIKE', '%'.$search_line.'%')->orderBy('date', 'desc')->paginate(8);
                    			return $results;     
                 			break;
                 			
                 			default:
                 				$results = $this->where('ua_name', 'LIKE', '%'.$search_line.'%')->orwhere('ru_name', 'LIKE', '%'.$search_line.'%')->orwhere('en_name', 'LIKE', '%'.$search_line.'%')->paginate(8);
                    			return $results;  
                 			break;
                 		}
                              	
                 

	 	}

	 public function getProductById(int $id){
	 	return $this->where('id', '=', $id)->first();
	 }	

	 public static function getProductName(int $id){
	 	$product = DB::table('product')->where('id', '=', $id)->first()->ru_name;
	  	return $product;
	 }
	 	
// 	 	// open an image file
// $img = Image::make('public/foo.jpg');

// // now you are able to resize the instance
// $img->resize(320, 240);

// // and insert a watermark for example
// $img->insert('public/watermark.png');

// // finally we save the image as a new file
// $img->save('public/bar.jpg');

	 



	 public function product_images(){
	 	 return $this->hasMany('App\Models\Product_images', 'product_id');
	 }

	 public function product_model(){
	 	 return $this->hasMany('App\Models\Dmodel', 'product_id');
	 }

	 public function product_schema(){
	 	 return $this->hasMany('App\Models\Product_schema', 'product_id');
	 }
	  public function product_icons(){
	 	 return $this->hasMany('App\Models\Product_images_icon', 'product_id');
	 }
	  public function main_category()
	{
    		return $this->belongsTo('App\Models\Main_category', 'category');
	}

	 public function sub_category()
	{
    		return $this->belongsTo('App\Models\Sub_category', 'sub_category');
	}

}
