<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryService extends Model
{
    
     protected $table = 'delivery_services';
	 public $timestamps = false;
	 protected $fillable = ['ua_name', 'ru_name', 'en_name'];
}
