<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Main_page_slider extends Model
{
    protected $table = 'main_page_slider';
	public $timestamps = false;
	protected $fillable = ['image'];
}
