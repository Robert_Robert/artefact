<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RU_article extends Model
{
    
     protected $table = 'ru_articles';
	 public $timestamps = false;
	 protected $fillable = ['ru_name', 'ru_text', 'date'];
 
	
}