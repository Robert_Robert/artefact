<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Product;

class Product_images extends Model
{
     protected $table = 'product_images';
	 public $timestamps = false;
	 protected $fillable = ['product_id', 'image'];

	 public function product()
	{
    		return $this->belongsTo('App\Models\Product');
	}
}
