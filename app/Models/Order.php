<?php

namespace App\Models;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Database\Eloquent\Model;
use  App\Models\Product_images;
use App\Models\Product_schema;
use App\Models\Main_category;
use App\Models\Sub_category;
use App\Models\Product_images_icon;
use Intervention\Image\Facades\Image;
use DB;
use App\Models\Dmodel;

class Order extends Model
{
  protected $table = 'orders';
  public $timestamps = false;
  protected $fillable = ['user_mail', 'order_price', 'status', 'created_at'];

  public function order_products(){
   return $this->hasMany('App\Models\Order_product', 'order_id');
  }

  public function order_delivery_info(){
   return $this->hasMany('App\Models\Order_delivery_info', 'order_id');
  }

 

}
