<?php

namespace App\Models;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Database\Eloquent\Model;

class Sub_category extends Model
{
     protected $table = 'sub_category';
	 public $timestamps = false;
	 protected $fillable = ['ua_name', 'ru_name', 'en_name', 'main_category'];
 
	 public function add_category(Request $request){

	 	$this->ua_name = $request->ua_name;
	 	$this->ru_name = $request->ru_name;
	 	$this->en_name = $request->en_name;
	 	$this->main_category = $request->main_category;


	 	$this->save();
	 }

	   public function product(){
	 	 return $this->hasMany('App\Models\Product', 'sub_category');
	 }
	  public function main_category()
	{
    		return $this->belongsTo('App\Models\Main_category', 'main_category');
	}
}
