<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

/*mainpage controller*/
 Route::get('/', 'MainPageController@index');
 Route::get('/change-language/{locale}', 'MainPageController@change_language');

 /*end mainpage controller*/


/*Blog*/
 Route::get('blog', 'Blog_controller@blog');
 Route::get('blog/single-article', 'Blog_controller@single_article');
/*endBlog*/

/*Contacts*/
 Route::get('contacts', 'MainPageController@contacts');
 Route::post('contacts/sendmail', 'MainPageController@contacts_send_mail');
/*EndContacts*/

/* category*/
 Route::get('/category/{main_cat}', 'CategoryController@show_category_all');
 Route::get('/category/{main_cat}/{sub_cat}', 'CategoryController@show_sub_category');
/*end category*/

/*product*/
 Route::get('/product/{product}', 'ProductController@show_product');
/*end product*/

#Shop
 Route::get('shop', 'CategoryController@shop');

#search
 Route::get('search', 'ProductController@search');

#add_to_bakset ajax
 Route::post('add-to-basket', 'ProductController@add_to_basket');

#get product price ajax
Route::post('get_product_price/{product}', 'BasketController@get_product_price');


 #delete from basket ajax
 Route::post('delete-from-basket', 'BasketController@delete_from_basket');

 #basket
 Route::get('basket', 'BasketController@show_basket');

 #basket_client_info
 Route::get('client_info', 'BasketController@client_info');

 #add basket to session
 Route::post('add_basket_to_session', 'BasketController@add_basket_to_session');

 #clear_basket_in_session
 Route::post('clear_basket_in_session', 'BasketController@clear_basket_in_session');

 #add_client_info
 Route::post('add_client_info', 'BasketController@add_client_info');

 #pay page
 Route::get('pay_page', 'BasketController@create_pay_page');

 #send pay request
 Route::get('pay-request', 'BasketController@pay_request');








Auth::routes();

Route::get('/home', 'HomeController@index');




// Route::group(['namespace' => 'Admin'], function() {
//     // Controllers Within The "App\Http\Controllers\Admin" Namespace
//     Route::get('admin', 'AdminController@index');

// });





/*Admin routes*/
 Route::get('admin', 'AdminController@index');
 Route::get('admin/exit', 'AdminController@ext');

 Route::group(['middleware' => 'ownAuth'], function()
	{


  ///////////Delivery Services

  Route::get('admin/delivery', 'AdminController@delivery_services');
  ////////// Orders history

  ////////// Add deliveri service
  Route::post('/admin/add_delivery_sevice', 'AdminController@add_delivery_sevice');
  Route::get('admin/delete-service/{service}', 'AdminController@delete_service');

  

  Route::get('admin/orders_history', 'AdminController@orders_history');	
  Route::get('admin/order-details/{order}', 'AdminController@order_detail');	
  	
  ///////// show edit-delivery options
  Route::get('admin/edit-order-delivery/{delivery}', 'AdminController@show_delivery_form');
  Route::post('admin/save_delivery_changes/{delivery}', 'AdminController@save_delivery_changes');

  //////Statistics routes
  Route::get('admin/date_statistic', 'AdminController@date_statistic');
  Route::get('admin/client_statistic', 'AdminController@client_statistic');

  ///////////Discount
  Route::get('admin/dicount_form', 'AdminController@discount_view');
  Route::post('admin/add_discount', 'AdminController@add_discount');
  Route::get('admin/discount_list', 'AdminController@discount_list');
  Route::get('admin/delete-discount/{discount}', 'AdminController@delete_discount');
  Route::get('admin/edit_dicount_form/{discount}', 'AdminController@edit_discount_form');
  Route::post('admin/edit_discount/{discount}', 'AdminController@edit_discount');


		 //AJAX_ADMIN
	 Route::post('admin/get-sub-category', 'AdminController@ajax_sub_category');
	 Route::post('admin/del_image/{id}', 'AdminController@delete_image');
	 Route::post('admin/del_slide/{id}', 'AdminController@delete_slide');
	 Route::post('admin/del_schema/{id}', 'AdminController@delete_schema');
	 Route::post('admin/del_model/{id}', 'AdminController@delete_model');
	 Route::post('admin/save_image', 'AdminController@save_image');
	 Route::post('/admin/save_main_slide', 'AdminController@save_slide');
	 Route::post('/admin/del_slide', 'AdminController@delete_main_slide');
	 ////////

	 #update slide of main slider
	 Route::post('admin/save_main_slide/{slide}', 'AdminController@update_slide_main_slider');


	/////add product
	 Route::get('admin/product', 'AdminController@product');
	 Route::post('admin/add-product', 'AdminController@add_product');
	 ///////edit product

	  Route::get('admin/products_list/{category}', 'AdminController@products_list');
	  Route::get('admin/edit_product/{product}', 'AdminController@edit_product');
	  Route::post('admin/save_change_product/{product}', 'AdminController@save_change_product');
	 /////////////////
	 ////////////////delete product
	  Route::get('admin/delete_product/{product}', 'AdminController@delete_product');
	  //////////////
	/////add category
	 Route::get('admin/category', 'AdminController@category');
	 Route::post('admin/add-category', 'AdminController@add_category');
	 ////// edit category

	 Route::get('admin/edit_category/{id}', 'AdminController@show_category');
	 Route::post('admin/edit_category/save-change/{category}', 'AdminController@save_change_category');
	 Route::get('admin/category-list', 'AdminController@category_list');
	 ////////
	 ///////delete category
	 Route::get('admin/delete_category/{id}', 'AdminController@category_delete');
	 ///////
	/////ad sub category
	 Route::get('admin/sub-category', 'AdminController@sub_category');
	 Route::post('admin/add-sub-category', 'AdminController@add_sub_category');
	 //BLOG

	 #main page slider
	 Route::get('admin/slider', 'AdminController@slider');
	 Route::post('admin/slider', 'AdminController@add_sliders');

	 Route::get('admin/main_slider', 'AdminController@main_slider');



	 ///ua article
	 Route::get('admin/blog/ua-article', 'Blog_controller@ua_article');
	 Route::post('admin/blog/add-ua-article', 'Blog_controller@add_ua_article');
	 Route::get('admin/blog/delete-ua-article/{id}', 'Blog_controller@delete_ua_article');
	 Route::get('admin/blog/ua-articles', 'Blog_controller@ua_articles');
	 Route::get('admin/blog/edit-ua-article/{id}', 'Blog_controller@edit_ua_article');
	 Route::post('admin/blog/edit-ua-article/{id}', 'Blog_controller@edit_ua');
	 ////////ru article
	 Route::get('admin/blog/ru-article', 'Blog_controller@ru_article');
	 Route::post('admin/blog/add-ru-article', 'Blog_controller@add_ru_article');
	 Route::get('admin/blog/ru-articles', 'Blog_controller@ru_articles');
	 Route::get('admin/blog/edit-ru-article/{id}', 'Blog_controller@edit_ru_article');
	 Route::post('admin/blog/edit-ru-article/{id}', 'Blog_controller@edit_ru');
	 Route::get('admin/blog/delete-ru-article/{id}', 'Blog_controller@delete_ru_article');
	////////en article
	 Route::get('admin/blog/en-article', 'Blog_controller@en_article');
	 Route::post('admin/blog/add-en-article', 'Blog_controller@add_en_article');
	 Route::get('admin/blog/en-articles', 'Blog_controller@en_articles');
	 Route::get('admin/blog/delete-en-article/{id}', 'Blog_controller@delete_en_article');
	 Route::get('admin/blog/edit-en-article/{id}', 'Blog_controller@edit_en_article');
	 Route::post('admin/blog/edit-en-article/{id}', 'Blog_controller@edit_en');

	 /////////////
});
/*end admin routes*/
